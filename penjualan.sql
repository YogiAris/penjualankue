-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 08, 2020 at 03:21 PM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.3.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `penjualan`
--

-- --------------------------------------------------------

--
-- Table structure for table `tm_chef`
--

CREATE TABLE `tm_chef` (
  `id_chef` varchar(7) NOT NULL DEFAULT '',
  `nm_chef` varchar(30) DEFAULT NULL,
  `no_handphone` varchar(14) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tm_chef`
--

INSERT INTO `tm_chef` (`id_chef`, `nm_chef`, `no_handphone`) VALUES
('C002', 'Chef Arnold Poernomo', '6285811278508'),
('C003', 'Chef Marinka', '628980487816'),
('C004', 'Chef Juna', '62991288827');

-- --------------------------------------------------------

--
-- Table structure for table `tm_makanan`
--

CREATE TABLE `tm_makanan` (
  `kd_makanan` varchar(7) NOT NULL DEFAULT '',
  `nm_makanan` varchar(30) DEFAULT NULL,
  `kategori` varchar(15) DEFAULT NULL,
  `harga_satuan` int(7) DEFAULT NULL,
  `satuan` varchar(6) DEFAULT NULL,
  `ukuran` varchar(10) DEFAULT NULL,
  `stock` int(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tm_makanan`
--

INSERT INTO `tm_makanan` (`kd_makanan`, `nm_makanan`, `kategori`, `harga_satuan`, `satuan`, `ukuran`, `stock`) VALUES
('M001', 'Brownis', 'Basah', 200000, 'pcs', 'small', 6),
('M002', 'Kue Kacang', 'Kering', 600000, 'Ball', 'Big', 20),
('M003', 'Kue Keju Coklat', 'Basah', 400000, 'pcs', 'Smalll', 10);

-- --------------------------------------------------------

--
-- Table structure for table `tm_pelanggan`
--

CREATE TABLE `tm_pelanggan` (
  `id_pelanggan` varchar(7) NOT NULL DEFAULT '',
  `nm_pelanggan` varchar(30) DEFAULT NULL,
  `no_handphone` varchar(14) DEFAULT NULL,
  `alamat` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tm_pelanggan`
--

INSERT INTO `tm_pelanggan` (`id_pelanggan`, `nm_pelanggan`, `no_handphone`, `alamat`) VALUES
('P001', 'Yusuf Chandra', '6289691720383', 'Jakarta'),
('P002', 'Rudi Hamka', '628990814138', 'Bandung');

-- --------------------------------------------------------

--
-- Table structure for table `tm_user`
--

CREATE TABLE `tm_user` (
  `id_user` varchar(7) NOT NULL DEFAULT '0',
  `nm_user` varchar(30) DEFAULT NULL,
  `username` varchar(20) DEFAULT NULL,
  `password` varchar(40) DEFAULT NULL,
  `no_handphone` varchar(14) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tm_user`
--

INSERT INTO `tm_user` (`id_user`, `nm_user`, `username`, `password`, `no_handphone`) VALUES
('A001', 'Leonel Messi', 'admin', '161ebd7d45089b3446ee4e0d86dbcf92', '628569012345'),
('A002', 'Christiano Ronaldo', 'user', '161ebd7d45089b3446ee4e0d86dbcf92', '628909890000');

-- --------------------------------------------------------

--
-- Table structure for table `tran_detil_penjualan`
--

CREATE TABLE `tran_detil_penjualan` (
  `kd_penjualan` varchar(7) NOT NULL DEFAULT '',
  `kd_makanan` varchar(5) NOT NULL DEFAULT '',
  `quantity` int(2) DEFAULT NULL,
  `total` int(7) DEFAULT NULL,
  `status_stok` varchar(15) DEFAULT NULL,
  `status_proses` varchar(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tran_detil_penjualan`
--

INSERT INTO `tran_detil_penjualan` (`kd_penjualan`, `kd_makanan`, `quantity`, `total`, `status_stok`, `status_proses`) VALUES
('PJN001', 'M001', 4, 800000, 'Stok', 'Selesai'),
('PJN001', 'M002', 3, 1800000, 'Pembuatan', 'Selesai'),
('PJN002', 'M001', 5, 1000000, 'Stok', 'Selesai'),
('PJN002', 'M002', 6, 3600000, 'Pembuatan', 'Proses'),
('PJN003', 'M001', 3, 600000, 'Stok', 'Selesai'),
('PJN003', 'M002', 4, 2400000, 'Pembuatan', 'Proses'),
('PJN005', 'M001', 2, 400000, 'Pembuatan', 'Proses'),
('PJN006', 'M001', 2, 400000, 'Stok', 'Selesai'),
('PJN006', 'M002', 2, 1200000, 'Pembuatan', 'Proses'),
('PJN007', 'M002', 2, 1200000, 'Pembuatan', 'Proses'),
('PJN008', 'M001', 30, 6000000, 'Pembuatan', 'Proses'),
('PJN008', 'M002', 2, 1200000, 'Pembuatan', 'Proses'),
('PJN009', 'M001', 30, 6000000, 'Pembuatan', 'Proses'),
('PJN009', 'M002', 2, 1200000, 'Pembuatan', 'Proses'),
('PJN010', 'M001', 30, 6000000, 'Pembuatan', 'Proses'),
('PJN010', 'M002', 2, 1200000, 'Pembuatan', 'Proses'),
('PJN011', 'M002', 20, 12000000, 'Pembuatan', 'Selesai'),
('PJN011', 'M003', 10, 4000000, 'Pembuatan', 'Selesai'),
('PJN012', 'M003', 10, 4000000, 'Pembuatan', 'Selesai'),
('PJN013', 'M003', 10, 4000000, 'Stok', 'Selesai'),
('PJN014', 'M001', 5, 1000000, 'Stok', 'Selesai'),
('PJN015', 'M001', 1, 200000, 'Stok', 'Selesai'),
('PJN017', 'M003', 5, 2000000, 'Stok', 'Selesai');

-- --------------------------------------------------------

--
-- Table structure for table `tran_penjualan`
--

CREATE TABLE `tran_penjualan` (
  `kd_penjualan` varchar(7) NOT NULL DEFAULT '',
  `tgl_penjualan` date DEFAULT NULL,
  `id_pelanggan` varchar(7) DEFAULT NULL,
  `id_chef` varchar(7) DEFAULT NULL,
  `jam_expired` datetime DEFAULT NULL,
  `keterangan_status` varchar(20) DEFAULT NULL,
  `konfirmasi_pembayaran` varchar(1000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tran_penjualan`
--

INSERT INTO `tran_penjualan` (`kd_penjualan`, `tgl_penjualan`, `id_pelanggan`, `id_chef`, `jam_expired`, `keterangan_status`, `konfirmasi_pembayaran`) VALUES
('PJN001', '2020-05-08', 'P001', 'C002', '2020-05-08 00:17:41', 'Belum Konfirmasi', ''),
('PJN002', '2020-05-08', 'P002', 'C002', '2020-05-08 01:42:09', 'Belum Konfirmasi', ''),
('PJN003', '2020-05-08', 'P001', 'C003', '2020-05-08 01:42:57', 'Belum Konfirmasi', ''),
('PJN005', '2020-05-08', 'P002', 'C002', '2020-05-08 02:58:21', 'Belum Konfirmasi', ''),
('PJN006', '2020-05-08', 'P002', 'C003', '2020-05-08 03:01:23', 'Belum Konfirmasi', ''),
('PJN007', '2020-05-08', 'P002', 'C002', '2020-05-08 03:02:33', 'Belum Konfirmasi', ''),
('PJN008', '2020-05-08', 'P002', 'C003', '2020-05-08 03:04:16', 'Belum Konfirmasi', ''),
('PJN009', '2020-05-08', 'P002', 'C003', '2020-05-08 03:08:45', 'Belum Konfirmasi', ''),
('PJN010', '2020-05-08', 'P002', 'C002', '2020-05-08 03:09:52', 'Belum Konfirmasi', ''),
('PJN011', '2020-05-08', 'P002', 'C002', '2020-05-08 06:33:55', 'Lunas', ''),
('PJN012', '2020-05-08', 'P002', 'C002', '2020-05-08 18:32:00', 'Konfirmasi', ''),
('PJN013', '2020-05-08', 'P001', 'C004', '2020-05-08 18:38:58', 'Belum Konfirmasi', ''),
('PJN014', '2020-05-08', 'P001', 'C004', '2020-05-08 18:39:49', 'Belum Konfirmasi', ''),
('PJN015', '2020-05-08', 'P002', 'C004', '2020-05-08 18:41:14', 'Belum Konfirmasi', ''),
('PJN016', '2020-05-08', 'P001', 'C003', '2020-05-08 18:42:41', 'Konfirmasi', ''),
('PJN017', '2020-05-08', 'P002', 'C002', '2020-05-08 18:44:10', 'Konfirmasi', 'asdasdasdasdccccccccc');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tm_chef`
--
ALTER TABLE `tm_chef`
  ADD PRIMARY KEY (`id_chef`);

--
-- Indexes for table `tm_makanan`
--
ALTER TABLE `tm_makanan`
  ADD PRIMARY KEY (`kd_makanan`);

--
-- Indexes for table `tm_pelanggan`
--
ALTER TABLE `tm_pelanggan`
  ADD PRIMARY KEY (`id_pelanggan`);

--
-- Indexes for table `tm_user`
--
ALTER TABLE `tm_user`
  ADD PRIMARY KEY (`id_user`);

--
-- Indexes for table `tran_detil_penjualan`
--
ALTER TABLE `tran_detil_penjualan`
  ADD PRIMARY KEY (`kd_penjualan`,`kd_makanan`);

--
-- Indexes for table `tran_penjualan`
--
ALTER TABLE `tran_penjualan`
  ADD PRIMARY KEY (`kd_penjualan`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
