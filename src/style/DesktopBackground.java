package style;
import java.awt.Graphics;
import java.awt.Image;
import javax.swing.JDesktopPane;

public class DesktopBackground extends JDesktopPane {

    private Image image;

    public DesktopBackground(){
        super();
    }

    @Override
    protected void paintComponent(Graphics g) {
        try {
            image = new javax.swing.ImageIcon(getClass().getResource("/image/screen-0.jpg")).getImage();
            if (g != null) {
                g.drawImage(image,
                        (this.getSize().width - image.getWidth(null)) / 2,
                        (this.getSize().height - image.getHeight(null)) / 2 - 30,
                        null);
            }
        } catch (NullPointerException error) {
            System.out.println("Can't find images !!"+error);
        }
    }
}
