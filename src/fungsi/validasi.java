package fungsi;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

public class validasi extends PlainDocument{
public final int limit;
public String Chars;

public validasi(int limit,String Char) {
    this.limit = limit;
    this.Chars = Char;
}

public void masukinString(int offset, String str, AttributeSet attr) throws BadLocationException {
    if (str == null) {
        return;
    }
        for (int i = 0; i < str.length(); i++) {
            if (!Chars.contains(String.valueOf(str.charAt(i)))) {
            return;
            }
        }
    super.insertString(offset, str, attr);
    }

    @Override
    public void insertString(int offset, String str, AttributeSet attr) throws BadLocationException {
        if (str == null) {
        return;
    }
        if ((getLength() + str.length()) <= limit) {
        masukinString(offset, str, attr);
        }
    }

}
