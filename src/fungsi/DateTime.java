package fungsi;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class DateTime {
    public String showTimeNow() {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        String hasil = sdf.format(cal.getTime());
        return hasil;
    }
    public String showDateNow() {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("EEEEE dd MMMMM yyyy");
        String hasil = sdf.format(cal.getTime());
        return hasil;
    }
}
