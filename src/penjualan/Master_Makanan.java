package penjualan;

import fungsi.validasi;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;
import koneksi.koneksi;

public class Master_Makanan extends javax.swing.JInternalFrame {

    koneksi database = new koneksi();
    Connection con;
    Statement stat;
    ResultSet rs;
    String sql;
    String data[] = new String[7];

    public Master_Makanan() {
        initComponents();
        table_makanan.setModel(tblModel);//membuat tabel
        Tabel(table_makanan, new int[]{60, 90, 70, 80, 70, 70,70});
        // set location
        Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
        setLocation(d.width / 2 - getWidth() / 2, d.height / 6 - getHeight() / 6);
        Validasi();
        set_awal();
        set_autonumber();
        tampilkan_data_makanan();
    }

    private void tampilkan_data_makanan() {
        hapus_tabel_makanan();
        try {
            con = database.open_a_Connection();
            stat = con.createStatement();
            sql = "SELECT * FROM tm_makanan ORDER BY kd_makanan DESC";
            rs = stat.executeQuery(sql);
            while (rs.next()) {
                data[0] = rs.getString("kd_makanan");
                data[1] = rs.getString("nm_makanan");
                data[2] = rs.getString("kategori");
                data[3] = rs.getString("harga_satuan");
                data[4] = rs.getString("satuan");
                data[5] = rs.getString("ukuran");
                data[6] = rs.getString("stock");
                tblModel.addRow(data);
            }
            stat.close();
            con.close();
        } catch (SQLException error) {
            System.err.print(error);
        }
    }
    
     private void set_autonumber() {
        try {
            con  = database.open_a_Connection();
            stat = con.createStatement();
            sql  = "SELECT RIGHT(kd_makanan,3) AS kd FROM tm_makanan";
            rs  = stat.executeQuery(sql);
            if (rs.first() == false) {
                txt_kode_makanan.setText("M001");
            } else {
                rs.last();
                int no = rs.getInt(1) + 1;
                String cno = String.valueOf(no);
                int pjg_cno = cno.length();
                for (int i = 0; i < 3 - pjg_cno; i++) {
                    cno = "0" + cno;
                }
                txt_kode_makanan.setText("M" + cno);
            }
            rs.close();
            stat.close();
            con.close();
        } catch (SQLException error) {
            System.out.println(error);
        }
    }

    private void set_awal() {
        btn_simpan.setEnabled(true);
        btn_ubah.setEnabled(false);
        btn_batal.setEnabled(true);
        btn_hapus.setEnabled(false);
        btn_keluar.setEnabled(true);
        txt_kode_makanan.setEnabled(false);

        txt_nama_makanan.requestFocus();
    }

    private void set_kosong() {
        txt_nama_makanan.setText("");
        txt_kategori.setText("");
        txt_harga_satuan.setText("");
        txt_satuan.setText("");
        txt_ukuran.setText("");
        txt_stock.setText("");
    }
    
    private void Validasi(){
        txt_harga_satuan.setDocument(new validasi(14, "0123456789"));// set validasi input
        txt_stock.setDocument(new validasi(14, "0123456789"));// set validasi input
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        desktopBackground1 = new style.DesktopBackground();
        jLabel1 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        txt_nama_makanan = new javax.swing.JTextField();
        txt_kategori = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        txt_harga_satuan = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        btn_simpan = new javax.swing.JButton();
        btn_ubah = new javax.swing.JButton();
        btn_hapus = new javax.swing.JButton();
        btn_batal = new javax.swing.JButton();
        btn_keluar = new javax.swing.JButton();
        txt_satuan = new javax.swing.JTextField();
        txt_ukuran = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        txt_stock = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        txt_kode_makanan = new javax.swing.JTextField();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        table_makanan = new javax.swing.JTable();
        cmb_cari_barang = new javax.swing.JComboBox();
        txt_cari_barang = new javax.swing.JTextField();
        btn_cari = new javax.swing.JButton();

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("MAKANAN");
        desktopBackground1.add(jLabel1);
        jLabel1.setBounds(4, 4, 460, 50);

        jPanel1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel1.setOpaque(false);

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel2.setText("Nama Makanan");

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel3.setText("Kategori");

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel4.setText("Satuan");

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel5.setText("Harga Satuan");

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel6.setText("Ukuran");

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Button", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 11))); // NOI18N
        jPanel3.setOpaque(false);

        btn_simpan.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btn_simpan.setText("Simpan");
        btn_simpan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_simpanActionPerformed(evt);
            }
        });

        btn_ubah.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btn_ubah.setText("Ubah");
        btn_ubah.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_ubahActionPerformed(evt);
            }
        });

        btn_hapus.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btn_hapus.setText("Hapus");
        btn_hapus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_hapusActionPerformed(evt);
            }
        });

        btn_batal.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btn_batal.setText("Batal");
        btn_batal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_batalActionPerformed(evt);
            }
        });

        btn_keluar.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btn_keluar.setText("Keluar");
        btn_keluar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_keluarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap(19, Short.MAX_VALUE)
                .addComponent(btn_simpan)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btn_ubah)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btn_hapus)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btn_batal)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btn_keluar)
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btn_simpan)
                    .addComponent(btn_ubah)
                    .addComponent(btn_hapus)
                    .addComponent(btn_batal)
                    .addComponent(btn_keluar))
                .addGap(0, 10, Short.MAX_VALUE))
        );

        jLabel7.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel7.setText("Stock");

        jLabel8.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel8.setText("Kode Makanan");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(36, 36, 36)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel5, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 92, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel8, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(txt_harga_satuan)
                    .addComponent(txt_kategori)
                    .addComponent(txt_nama_makanan)
                    .addComponent(txt_satuan)
                    .addComponent(txt_ukuran)
                    .addComponent(txt_stock, javax.swing.GroupLayout.DEFAULT_SIZE, 174, Short.MAX_VALUE)
                    .addComponent(txt_kode_makanan))
                .addGap(63, 63, 63))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap(16, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txt_kode_makanan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txt_nama_makanan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txt_kategori, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txt_harga_satuan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txt_satuan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txt_ukuran, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txt_stock, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        desktopBackground1.add(jPanel1);
        jPanel1.setBounds(20, 50, 420, 300);

        jPanel2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel2.setOpaque(false);

        table_makanan.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        table_makanan.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                table_makananMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(table_makanan);

        cmb_cari_barang.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Kode Makanan", "Nama Makanan", "Kategori" }));

        btn_cari.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btn_cari.setText("Cari");
        btn_cari.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_cariActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(cmb_cari_barang, javax.swing.GroupLayout.PREFERRED_SIZE, 139, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txt_cari_barang)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_cari))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txt_cari_barang)
                        .addComponent(btn_cari, javax.swing.GroupLayout.DEFAULT_SIZE, 25, Short.MAX_VALUE))
                    .addComponent(cmb_cari_barang, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 230, Short.MAX_VALUE)
                .addContainerGap())
        );

        desktopBackground1.add(jPanel2);
        jPanel2.setBounds(20, 360, 420, 290);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(desktopBackground1, javax.swing.GroupLayout.DEFAULT_SIZE, 467, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(desktopBackground1, javax.swing.GroupLayout.DEFAULT_SIZE, 686, Short.MAX_VALUE)
        );

        setSize(new java.awt.Dimension(483, 716));
    }// </editor-fold>//GEN-END:initComponents

    private void btn_keluarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_keluarActionPerformed
        dispose();
    }//GEN-LAST:event_btn_keluarActionPerformed

    private void btn_simpanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_simpanActionPerformed
        if (txt_nama_makanan.getText().isEmpty()
                | txt_kategori.getText().isEmpty()
                | txt_harga_satuan.getText().isEmpty()
                | txt_satuan.getText().isEmpty()
                | txt_ukuran.getText().isEmpty()
                | txt_stock.getText().isEmpty()) {
            JOptionPane.showMessageDialog(null, "Form masih ada yang kosong", " Peringatan ", JOptionPane.WARNING_MESSAGE);
            return;
        }
        try {
            con = database.open_a_Connection();
            stat = con.createStatement();
            sql = " INSERT INTO tm_makanan VALUES ('" + txt_kode_makanan.getText() + "',"
                    + "'" + txt_nama_makanan.getText() + "',"
                    + "'" + txt_kategori.getText() + "',"
                    + "'" + txt_harga_satuan.getText() + "',"
                    + "'" + txt_satuan.getText() + "',"
                    + "'" + txt_ukuran.getText() + "',"
                    + "'" + txt_stock.getText() + "')";
            int sukses = stat.executeUpdate(sql);
            if (sukses == 1) {
                JOptionPane.showMessageDialog(null, "Berhasil simpan data", " Informasi ", JOptionPane.INFORMATION_MESSAGE);
                set_awal();
                set_kosong();
                set_autonumber();
                tampilkan_data_makanan();
            }
            stat.close();
            con.close();
        } catch (SQLException error) {
            System.err.println(error);
        }
    }//GEN-LAST:event_btn_simpanActionPerformed
    
    private void btn_ubahActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_ubahActionPerformed
        if (txt_nama_makanan.getText().isEmpty()
                | txt_kategori.getText().isEmpty()
                | txt_harga_satuan.getText().isEmpty()
                | txt_satuan.getText().isEmpty()
                | txt_ukuran.getText().isEmpty()
                | txt_stock.getText().isEmpty()) {
            JOptionPane.showMessageDialog(null, "Form masih ada yang kosong", "Peringatan", JOptionPane.WARNING_MESSAGE);
            return;
        }
        
        try {
            con  = database.open_a_Connection();
            stat = con.createStatement();
            sql  = " UPDATE tm_makanan SET "
                    + "nm_makanan='" + txt_nama_makanan.getText() + "',"
                    + "kategori='" + txt_kategori.getText() + "',"
                    + "harga_satuan='" + txt_harga_satuan.getText() + "',"
                    + "satuan='" + txt_satuan.getText() + "',"
                    + "ukuran='" + txt_ukuran.getText() + "',"
                    + "stock='" + txt_stock.getText() + "'"
                    + "WHERE kd_makanan='" + txt_kode_makanan.getText() + "'";
            int sukses = stat.executeUpdate(sql);
            if (sukses == 1) {
                JOptionPane.showMessageDialog(null, "Data berhasil diubah", "INFORMASI", JOptionPane.INFORMATION_MESSAGE);
                set_awal();
                set_kosong();
                set_autonumber();
                tampilkan_data_makanan();
            }
            stat.close();
            con.close();
        } catch (SQLException error) {
            System.err.print(error);
        }
    }//GEN-LAST:event_btn_ubahActionPerformed

    private void btn_hapusActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_hapusActionPerformed
        try {
            int jawab = JOptionPane.showConfirmDialog(null, "Anda yakin menghapus Data?", "Konfirmasi", JOptionPane.YES_NO_OPTION);
            if (jawab == 0) {
                con = database.open_a_Connection();
                stat = con.createStatement();
                sql = "DELETE FROM tm_makanan WHERE kd_makanan='" + txt_kode_makanan.getText() + "'";
                int sukses = stat.executeUpdate(sql);
                if (sukses == 1) {
                    JOptionPane.showMessageDialog(null, "Data berhasil dihapus", "INFORMASI", JOptionPane.INFORMATION_MESSAGE);
                    tblModel.removeRow(row);
                    set_awal();
                    set_kosong();
                }
            }
            con.close();
            stat.close();
        } catch (SQLException error) {
            System.out.println(error);
            JOptionPane.showMessageDialog(null, "Data gagal di hapus" + error, "Peringatan", JOptionPane.WARNING_MESSAGE);
        }
    }//GEN-LAST:event_btn_hapusActionPerformed

    private void btn_batalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_batalActionPerformed
        set_kosong();
        set_awal();
        set_autonumber();
    }//GEN-LAST:event_btn_batalActionPerformed

    private void btn_cariActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_cariActionPerformed
        hapus_tabel_makanan();
        String kata_kunci = "";
        if (cmb_cari_barang.getSelectedItem().toString().equals("Kode Makanan")) {
            kata_kunci = "kd_makanan";
        } else if (cmb_cari_barang.getSelectedItem().toString().equals("Nama Makanan")) {
            kata_kunci = "nm_makanan";
        } else if (cmb_cari_barang.getSelectedItem().toString().equals("Kategori")) {
            kata_kunci = "kategori";
        }
        try {
            con = database.open_a_Connection();
            stat = con.createStatement();
            sql = "SELECT * FROM tm_makanan WHERE " + kata_kunci + " LIKE '%" + txt_cari_barang.getText() + "%' ORDER BY kd_makanan DESC";
            rs = stat.executeQuery(sql);
            while (rs.next()) {
                data[0] = rs.getString("kd_makanan");
                data[1] = rs.getString("nm_makanan");
                data[2] = rs.getString("kategori");
                data[3] = rs.getString("harga_satuan");
                data[4] = rs.getString("satuan");
                data[5] = rs.getString("ukuran");
                data[6] = rs.getString("stock");
                tblModel.addRow(data);
            }
            stat.close();
            con.close();
        } catch (SQLException error) {
            System.err.print(error);
        }
    }//GEN-LAST:event_btn_cariActionPerformed
    private int row;
    private void table_makananMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_table_makananMouseClicked
        if (evt.getClickCount() == 2) {
            row = table_makanan.getSelectedRow();
            txt_kode_makanan.setText(tblModel.getValueAt(row, 0).toString());
            txt_nama_makanan.setText(tblModel.getValueAt(row, 1).toString());
            txt_kategori.setText(tblModel.getValueAt(row, 2).toString());
            txt_harga_satuan.setText(tblModel.getValueAt(row, 3).toString());
            txt_satuan.setText(tblModel.getValueAt(row, 4).toString());
            txt_ukuran.setText(tblModel.getValueAt(row, 5).toString());
            txt_stock.setText(tblModel.getValueAt(row, 6).toString());

            btn_simpan.setEnabled(false);
            btn_hapus.setEnabled(true);
            btn_ubah.setEnabled(true);
            btn_batal.setEnabled(true);
            btn_batal.setEnabled(true);
        }
    }//GEN-LAST:event_table_makananMouseClicked


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_batal;
    private javax.swing.JButton btn_cari;
    private javax.swing.JButton btn_hapus;
    private javax.swing.JButton btn_keluar;
    private javax.swing.JButton btn_simpan;
    private javax.swing.JButton btn_ubah;
    private javax.swing.JComboBox cmb_cari_barang;
    private style.DesktopBackground desktopBackground1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable table_makanan;
    private javax.swing.JTextField txt_cari_barang;
    private javax.swing.JTextField txt_harga_satuan;
    private javax.swing.JTextField txt_kategori;
    private javax.swing.JTextField txt_kode_makanan;
    private javax.swing.JTextField txt_nama_makanan;
    private javax.swing.JTextField txt_satuan;
    private javax.swing.JTextField txt_stock;
    private javax.swing.JTextField txt_ukuran;
    // End of variables declaration//GEN-END:variables
    private void Tabel(javax.swing.JTable tb, int lebar[]) {
        tb.setAutoResizeMode(tb.AUTO_RESIZE_OFF);
        int kolom = tb.getColumnCount();
        for (int i = 0; i < kolom; i++) {
            javax.swing.table.TableColumn tbc = tb.getColumnModel().getColumn(i);
            tbc.setPreferredWidth(lebar[i]);
            tb.setRowHeight(17);
        }
    }

    private void hapus_tabel_makanan() {
        int rowCount = table_makanan.getRowCount();
        for (int i = 0; i < rowCount; i++) {
            tblModel.removeRow(0);
        }
    }

    private javax.swing.table.DefaultTableModel tblModel = getDefaultTableModel();

    private javax.swing.table.DefaultTableModel getDefaultTableModel() {
        return new javax.swing.table.DefaultTableModel(
                new Object[][]{},
                new String[]{"Kd Makanan","Nama Makanan", "Kategori", "Harga Satuan", "Satuan" ,"Ukuran" ,"Stock"}
        ) {
            boolean[] canEdit = new boolean[]{
                false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit[columnIndex];
            }
        };
    }
}
