package penjualan;

import fungsi.validasi;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;
import koneksi.koneksi;

public class Master_User extends javax.swing.JInternalFrame {
    

    koneksi database = new koneksi();
    Connection con;
    Statement stat;
    ResultSet rs;
    String sql;
    String data[] = new String[5];
    
    public Master_User() {
        initComponents();
        // set location
        table_admin.setModel(tblModel);//membuat tabel
        Tabel(table_admin, new int[]{70, 120, 120, 100, 100});
        Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
        setLocation(d.width / 2 - getWidth() / 2, d.height / 6 - getHeight() / 6);
        tampilkan_data_user();
        Validasi();
        set_awal();
        set_autonumber();
        
    }
    
    private void tampilkan_data_user() {
        try {
            con = database.open_a_Connection();
            stat = con.createStatement();
            sql = "SELECT * FROM tm_user ORDER BY id_user DESC";
            rs = stat.executeQuery(sql);
            while (rs.next()) {
                data[0] = rs.getString("id_user");
                data[1] = rs.getString("nm_user");
                data[2] = rs.getString("username");
                data[3] = rs.getString("no_handphone");
                tblModel.addRow(data);
            }
            stat.close();
            con.close();
        } catch (SQLException error) {
            System.err.print(error);
        }
    }
    
    private void set_autonumber() {
        try {
            con  = database.open_a_Connection();
            stat = con.createStatement();
            sql  = "SELECT RIGHT(id_user,3) AS kd FROM tm_user";
            rs  = stat.executeQuery(sql);
            if (rs.first() == false) {
                txt_id_user.setText("A001");
            } else {
                rs.last();
                int no = rs.getInt(1) + 1;
                String cno = String.valueOf(no);
                int pjg_cno = cno.length();
                for (int i = 0; i < 3 - pjg_cno; i++) {
                    cno = "0" + cno;
                }
                txt_id_user.setText("A" + cno);
            }
            rs.close();
            stat.close();
            con.close();
        } catch (SQLException error) {
            System.out.println(error);
        }
    }
    
    private void set_awal(){
        btn_simpan.setEnabled(true);
        btn_ubah.setEnabled(false);
        btn_batal.setEnabled(false);
        btn_hapus.setEnabled(false);
        btn_keluar.setEnabled(true);
        
        txt_id_user.setEnabled(false);
        
        txt_no_hp.setText("62");
    }
    
    private void set_kosong(){
        txt_nama_user.setText("");
        txt_nama_pengguna.setText("");
        txt_password.setText("");
        txt_ulangi_password.setText("");
        
        txt_no_hp.setText("62");
    }
    
    private void Validasi (){
        txt_no_hp.setDocument(new validasi(14, "0123456789"));// set validasi input
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        desktopBackground1 = new style.DesktopBackground();
        jLabel1 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        txt_nama_user = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        txt_nama_pengguna = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        btn_simpan = new javax.swing.JButton();
        btn_ubah = new javax.swing.JButton();
        btn_hapus = new javax.swing.JButton();
        btn_batal = new javax.swing.JButton();
        btn_keluar = new javax.swing.JButton();
        txt_password = new javax.swing.JPasswordField();
        txt_ulangi_password = new javax.swing.JPasswordField();
        jLabel3 = new javax.swing.JLabel();
        txt_no_hp = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        txt_id_user = new javax.swing.JTextField();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        table_admin = new javax.swing.JTable();
        cmb_cari_admin = new javax.swing.JComboBox();
        txt_cari_admin = new javax.swing.JTextField();
        btn_cari = new javax.swing.JButton();

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("USER");
        desktopBackground1.add(jLabel1);
        jLabel1.setBounds(4, 4, 460, 50);

        jPanel1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel1.setOpaque(false);

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel2.setText("Nama User");

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel4.setText("Kata Kunci");

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel5.setText("Nama Pengguna");

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel6.setText("Ulangi Kata Kunci");

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Button", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 11))); // NOI18N
        jPanel3.setOpaque(false);

        btn_simpan.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btn_simpan.setText("Simpan");
        btn_simpan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_simpanActionPerformed(evt);
            }
        });

        btn_ubah.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btn_ubah.setText("Ubah");
        btn_ubah.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_ubahActionPerformed(evt);
            }
        });

        btn_hapus.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btn_hapus.setText("Hapus");
        btn_hapus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_hapusActionPerformed(evt);
            }
        });

        btn_batal.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btn_batal.setText("Batal");
        btn_batal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_batalActionPerformed(evt);
            }
        });

        btn_keluar.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btn_keluar.setText("Keluar");
        btn_keluar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_keluarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btn_simpan)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btn_ubah)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btn_hapus)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btn_batal)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btn_keluar)
                .addContainerGap(19, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btn_simpan)
                    .addComponent(btn_ubah)
                    .addComponent(btn_hapus)
                    .addComponent(btn_batal)
                    .addComponent(btn_keluar))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel3.setText("No Handphone");

        jLabel7.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel7.setText("Id User");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addContainerGap())
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGap(25, 25, 25)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 106, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(txt_no_hp, javax.swing.GroupLayout.PREFERRED_SIZE, 174, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jLabel5, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jLabel6)
                                    .addComponent(jLabel7, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(txt_nama_pengguna)
                                    .addComponent(txt_nama_user)
                                    .addComponent(txt_password)
                                    .addComponent(txt_ulangi_password, javax.swing.GroupLayout.DEFAULT_SIZE, 174, Short.MAX_VALUE)
                                    .addComponent(txt_id_user))))
                        .addGap(27, 27, 27))))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap(26, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txt_id_user, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txt_nama_user, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txt_nama_pengguna, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txt_password, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txt_ulangi_password, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txt_no_hp, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(9, 9, 9)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        desktopBackground1.add(jPanel1);
        jPanel1.setBounds(20, 60, 420, 290);

        jPanel2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel2.setOpaque(false);

        table_admin.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        table_admin.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                table_adminMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(table_admin);

        cmb_cari_admin.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Id User", "Nama User", "Username" }));

        btn_cari.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btn_cari.setText("Cari");
        btn_cari.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_cariActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(cmb_cari_admin, javax.swing.GroupLayout.PREFERRED_SIZE, 139, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txt_cari_admin)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_cari))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cmb_cari_admin, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txt_cari_admin)
                        .addComponent(btn_cari, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(13, 13, 13)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 208, Short.MAX_VALUE)
                .addContainerGap())
        );

        desktopBackground1.add(jPanel2);
        jPanel2.setBounds(20, 360, 420, 270);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(desktopBackground1, javax.swing.GroupLayout.DEFAULT_SIZE, 467, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(desktopBackground1, javax.swing.GroupLayout.DEFAULT_SIZE, 646, Short.MAX_VALUE)
        );

        setSize(new java.awt.Dimension(483, 676));
    }// </editor-fold>//GEN-END:initComponents

    private void btn_keluarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_keluarActionPerformed
        dispose();
    }//GEN-LAST:event_btn_keluarActionPerformed

    private void btn_simpanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_simpanActionPerformed
        if (txt_id_user.getText().isEmpty()
                |txt_nama_user.getText().isEmpty()
                | txt_nama_pengguna.getText().isEmpty()
                | txt_password.getText().isEmpty()
                | txt_ulangi_password.getText().isEmpty()
                | txt_no_hp.getText().isEmpty()) {
            JOptionPane.showMessageDialog(null, "Silahkan mengisi dengan data yang valid", " Peringatan ", JOptionPane.WARNING_MESSAGE);
            return;
        }
        if (txt_password.getText().equals(txt_ulangi_password.getText()) == false) {
            JOptionPane.showMessageDialog(null, "Masukan Password yang sama", " Peringatan ", JOptionPane.WARNING_MESSAGE);
            return;
        }
        try {
            con = database.open_a_Connection();
            stat = con.createStatement();
            sql = " INSERT INTO tm_user VALUES ('" + txt_id_user.getText() + "',"
                    + "'" + txt_nama_user.getText() + "',"
                    + "'" + txt_nama_pengguna.getText()+ "',"
                    + " md5 ('"+ txt_password.getText()
                    + "'), '" + txt_no_hp.getText() + "')" ;
            int sukses = stat.executeUpdate(sql);
            if (sukses == 1) {
                JOptionPane.showMessageDialog(null, "Berhasil simpan data", " Informasi ", JOptionPane.INFORMATION_MESSAGE);
                set_kosong();
                set_awal();
                hapus_tabel_user();
                tampilkan_data_user();
                set_autonumber();
            }
            stat.close();
            con.close();
        } catch (SQLException error) {
            System.err.println(error);
        }
    }//GEN-LAST:event_btn_simpanActionPerformed

    private void btn_ubahActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_ubahActionPerformed
        if (txt_nama_user.getText().isEmpty()
                | txt_nama_pengguna.getText().isEmpty()) {
            JOptionPane.showMessageDialog(null, "Silahkan mengisi dengan data yang valid", "Peringatan", JOptionPane.WARNING_MESSAGE);
            return;
        }
        if (txt_password.getText().equals(txt_ulangi_password.getText()) == false) {
            JOptionPane.showMessageDialog(null, "Masukan password yang sama", " Peringatan ", JOptionPane.WARNING_MESSAGE);
            return;
        }
        try {
            con = database.open_a_Connection();
            stat = con.createStatement();
            sql = " UPDATE tm_user SET "
                    + "nm_user='" + txt_nama_user.getText() + "',"
                    + "username='" + txt_nama_pengguna.getText() + "'"
                    + ((txt_password.getText().isEmpty() == false) ? ",password=md5('" + txt_password.getText() + "')" : "")
                    + ",no_handphone='" + txt_no_hp.getText() + "'"
                    + "WHERE id_user='" + txt_id_user.getText() + "'";
            int sukses = stat.executeUpdate(sql);
            if (sukses == 1) {
                JOptionPane.showMessageDialog(null, "Data berhasil diubah", "INFORMASI", JOptionPane.INFORMATION_MESSAGE);
                set_awal();
                set_kosong();
                hapus_tabel_user();
                tampilkan_data_user();
            }
            stat.close();
            con.close();
        } catch (SQLException error) {
            System.err.print(error);
        }
    }//GEN-LAST:event_btn_ubahActionPerformed

    private void btn_hapusActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_hapusActionPerformed
        try {
            int jawab = JOptionPane.showConfirmDialog(null, "Anda yakin akan menghapus??", "Konfirmasi", JOptionPane.YES_NO_OPTION);
            if (jawab == 0) {
                con = database.open_a_Connection();
                stat = con.createStatement();
                sql = "DELETE FROM tm_user WHERE id_user='" + txt_id_user.getText() + "'";
                int sukses = stat.executeUpdate(sql);
                if (sukses == 1) {
                    JOptionPane.showMessageDialog(null, "Berhasil menghapus data");
                    tblModel.removeRow(row);
                    set_awal();
                    set_kosong();
                }
            }
            stat.close();
            con.close();
        } catch (SQLException error) {
            System.err.print(error);
        }
    }//GEN-LAST:event_btn_hapusActionPerformed

    private void btn_batalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_batalActionPerformed
        set_kosong();
        hapus_tabel_user();
        tampilkan_data_user();
    }//GEN-LAST:event_btn_batalActionPerformed

    private void btn_cariActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_cariActionPerformed
        hapus_tabel_user();
        String kata_kunci = "";
        if (cmb_cari_admin.getSelectedItem().toString().equals("Id User")) {
            kata_kunci = "id_user";
        } else if (cmb_cari_admin.getSelectedItem().toString().equals("Nama User")) {
            kata_kunci = "nm_user";
        } else if (cmb_cari_admin.getSelectedItem().toString().equals("Username")) {
            kata_kunci = "username";
        }
        try {
            con = database.open_a_Connection();
            stat = con.createStatement();
            sql = "SELECT * FROM tm_user WHERE " + kata_kunci + " LIKE '%" + txt_cari_admin.getText() + "%' ORDER BY id_user DESC";
            rs = stat.executeQuery(sql);
            while (rs.next()) {
                data[0] = rs.getString("id_user");
                data[1] = rs.getString("nm_user");
                data[2] = rs.getString("username");
                tblModel.addRow(data);
            }
            stat.close();
            con.close();
        } catch (SQLException error) {
            System.err.print(error);
        }
    }//GEN-LAST:event_btn_cariActionPerformed
    
    private int row ;
    private void table_adminMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_table_adminMouseClicked
        if (evt.getClickCount() == 2) {
            row = table_admin.getSelectedRow();
            txt_id_user.setText(tblModel.getValueAt(row, 0).toString());
            txt_nama_user.setText(tblModel.getValueAt(row, 1).toString());
            txt_nama_pengguna.setText(tblModel.getValueAt(row, 2).toString());
            txt_no_hp.setText(tblModel.getValueAt(row, 3).toString());
        
            
            btn_simpan.setEnabled(false);
            btn_ubah.setEnabled(true);
            btn_hapus.setEnabled(true);
            btn_batal.setEnabled(true);
        }
    }//GEN-LAST:event_table_adminMouseClicked


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_batal;
    private javax.swing.JButton btn_cari;
    private javax.swing.JButton btn_hapus;
    private javax.swing.JButton btn_keluar;
    private javax.swing.JButton btn_simpan;
    private javax.swing.JButton btn_ubah;
    private javax.swing.JComboBox cmb_cari_admin;
    private style.DesktopBackground desktopBackground1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable table_admin;
    private javax.swing.JTextField txt_cari_admin;
    private javax.swing.JTextField txt_id_user;
    private javax.swing.JTextField txt_nama_pengguna;
    private javax.swing.JTextField txt_nama_user;
    private javax.swing.JTextField txt_no_hp;
    private javax.swing.JPasswordField txt_password;
    private javax.swing.JPasswordField txt_ulangi_password;
    // End of variables declaration//GEN-END:variables

    private void Tabel(javax.swing.JTable tb, int lebar[]) {
        tb.setAutoResizeMode(tb.AUTO_RESIZE_OFF);
        int kolom = tb.getColumnCount();
        for (int i = 0; i < kolom; i++) {
            javax.swing.table.TableColumn tbc = tb.getColumnModel().getColumn(i);
            tbc.setPreferredWidth(lebar[i]);
            tb.setRowHeight(17);
        }
    }

    private void hapus_tabel_user() {
        int rowCount = table_admin.getRowCount();
        for (int i = 0; i < rowCount; i++) {
            tblModel.removeRow(0);
        }
    }

    private javax.swing.table.DefaultTableModel tblModel = getDefaultTableModel();

    private javax.swing.table.DefaultTableModel getDefaultTableModel() {
        return new javax.swing.table.DefaultTableModel(
                new Object[][]{},
                new String[]{"Id User", "Nama User", "Username", "No handphone"}
        ) {
            boolean[] canEdit = new boolean[]{
                false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit[columnIndex];
            }
        };
    }
}
