package penjualan;

import fungsi.DateTime;
import java.awt.Toolkit;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JInternalFrame;
import koneksi.koneksi;

public class Menu extends javax.swing.JFrame {
    Master_Pelanggan pelanggan = new Master_Pelanggan(this);
    Trx_Penjualan penjualan = new Trx_Penjualan(this);
    DateTime livetiming = new DateTime();
    koneksi database = new koneksi();
    Connection con;
    Statement stat;
    ResultSet rs;
    String sql;
        
    int ScreenWidth, ScreenHeight;
    public Menu() {
        initComponents();
        ScreenWidth = (int) Toolkit.getDefaultToolkit().getScreenSize().getWidth();
        ScreenHeight = (int) Toolkit.getDefaultToolkit().getScreenSize().getHeight();
        setExtendedState(MAXIMIZED_BOTH);
    }
    

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        desktop = new style.DesktopBackground();
        jMenuBar1 = new javax.swing.JMenuBar();
        j_logout = new javax.swing.JMenu();
        J_Logout = new javax.swing.JMenuItem();
        m_master = new javax.swing.JMenu();
        m_user = new javax.swing.JMenuItem();
        m_makanan = new javax.swing.JMenuItem();
        m_pelanggan = new javax.swing.JMenuItem();
        m_chef = new javax.swing.JMenuItem();
        m_transaksi = new javax.swing.JMenu();
        j_transaksi_penjualan = new javax.swing.JMenuItem();
        m_laporan = new javax.swing.JMenu();
        j_laporan_pen = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setUndecorated(true);
        setResizable(false);

        desktop.setBackground(new java.awt.Color(204, 204, 204));

        j_logout.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/ico-file.png"))); // NOI18N
        j_logout.setText("File");

        J_Logout.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/ico-logout.png"))); // NOI18N
        J_Logout.setText("Logout");
        J_Logout.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                J_LogoutActionPerformed(evt);
            }
        });
        j_logout.add(J_Logout);

        jMenuBar1.add(j_logout);

        m_master.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/ico-master.png"))); // NOI18N
        m_master.setText("Master");

        m_user.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/ico-admin.png"))); // NOI18N
        m_user.setLabel("User");
        m_user.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                m_userActionPerformed(evt);
            }
        });
        m_master.add(m_user);

        m_makanan.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/ico-produk.png"))); // NOI18N
        m_makanan.setText("Makanan");
        m_makanan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                m_makananActionPerformed(evt);
            }
        });
        m_master.add(m_makanan);

        m_pelanggan.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/ico-pelanggan.png"))); // NOI18N
        m_pelanggan.setText("Pelanggan");
        m_pelanggan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                m_pelangganActionPerformed(evt);
            }
        });
        m_master.add(m_pelanggan);

        m_chef.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/ico-master.png"))); // NOI18N
        m_chef.setText("Chef");
        m_chef.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                m_chefActionPerformed(evt);
            }
        });
        m_master.add(m_chef);

        jMenuBar1.add(m_master);

        m_transaksi.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/ico-transaksi.png"))); // NOI18N
        m_transaksi.setText("Transaksi");

        j_transaksi_penjualan.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/ico-transaksi.png"))); // NOI18N
        j_transaksi_penjualan.setText("Penjualan");
        j_transaksi_penjualan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                j_transaksi_penjualanActionPerformed(evt);
            }
        });
        m_transaksi.add(j_transaksi_penjualan);

        jMenuBar1.add(m_transaksi);

        m_laporan.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/ico-report.png"))); // NOI18N
        m_laporan.setText("Laporan");

        j_laporan_pen.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/ico-report.png"))); // NOI18N
        j_laporan_pen.setText("Laporan Penjualan");
        j_laporan_pen.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                j_laporan_penActionPerformed(evt);
            }
        });
        m_laporan.add(j_laporan_pen);

        jMenuBar1.add(m_laporan);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(desktop, javax.swing.GroupLayout.DEFAULT_SIZE, 776, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(desktop, javax.swing.GroupLayout.DEFAULT_SIZE, 534, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void J_LogoutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_J_LogoutActionPerformed
        dispose();
        new Login().setVisible(true);
    }//GEN-LAST:event_J_LogoutActionPerformed

    JInternalFrame Admin;
    private void m_userActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_m_userActionPerformed
        try {
            Admin = new Master_User();
            desktop.add(Admin);
            Admin.setVisible(true);
        } catch (Exception error) {
            System.out.println(error);
        }
    }//GEN-LAST:event_m_userActionPerformed

    JInternalFrame Makanan;
    private void m_makananActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_m_makananActionPerformed
        try {
            Makanan = new Master_Makanan();
            desktop.add(Makanan);
            Makanan.setVisible(true);
        } catch (Exception error) {
            System.out.println(error);
        }
    }//GEN-LAST:event_m_makananActionPerformed

    private void m_pelangganActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_m_pelangganActionPerformed
        pelanggan.setVisible(true);
    }//GEN-LAST:event_m_pelangganActionPerformed

    
    JInternalFrame Trx_Kwitansi;
    JInternalFrame InOut;
    JInternalFrame Complain;
    JInternalFrame Lap_Pelanggan;
    JInternalFrame Lap_Penjualan;    JInternalFrame Chef ;    JInternalFrame Lap_Makanan ;
    private void m_chefActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_m_chefActionPerformed
        try {
            Chef = new Master_Chef();
            desktop.add(Chef);
            Chef.setVisible(true);
        } catch (Exception error) {
            System.out.println(error);
        }
    }//GEN-LAST:event_m_chefActionPerformed

    private void j_laporan_penActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_j_laporan_penActionPerformed
        try {
            Lap_Penjualan = new Laporan_Penjualan();
            desktop.add(Lap_Penjualan);
            Lap_Penjualan.setVisible(true);
        } catch (Exception error) {
            System.out.println(error);
        }
    }//GEN-LAST:event_j_laporan_penActionPerformed

    private void j_transaksi_penjualanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_j_transaksi_penjualanActionPerformed
        penjualan.setVisible(true);
        penjualan.set_kosong();
        penjualan.set_awal();
        penjualan.set_autonumber();
        penjualan.tampilkan_data_penjualan();
    }//GEN-LAST:event_j_transaksi_penjualanActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuItem J_Logout;
    private style.DesktopBackground desktop;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem j_laporan_pen;
    private javax.swing.JMenu j_logout;
    private javax.swing.JMenuItem j_transaksi_penjualan;
    private javax.swing.JMenuItem m_chef;
    private javax.swing.JMenu m_laporan;
    private javax.swing.JMenuItem m_makanan;
    private javax.swing.JMenu m_master;
    private javax.swing.JMenuItem m_pelanggan;
    private javax.swing.JMenu m_transaksi;
    private javax.swing.JMenuItem m_user;
    // End of variables declaration//GEN-END:variables
 

    public String cari_data_tabel(String tabel, String field, String search_param, String search_value){
        String value = "";
        try {
            try (Connection connect = database.open_a_Connection(); Statement state = connect.createStatement()) {
                String sqls = "SELECT * FROM "+tabel+" WHERE "+search_param+"='"+search_value+"'";
                ResultSet res = state.executeQuery(sqls);
                if (res.next() == false) {
                    value = "";
                }else{
                    value = res.getString(field);
                }
            }
        } catch (SQLException error) {
            System.err.println("cari_data_tabel::"+error);
        }
        return value;
    }
}
