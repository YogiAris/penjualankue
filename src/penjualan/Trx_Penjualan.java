package penjualan;

import fungsi.validasi;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.io.File;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JOptionPane;
import koneksi.koneksi;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.view.JasperViewer;

public class Trx_Penjualan extends javax.swing.JDialog {

    koneksi database = new koneksi();
    Connection con;
    Statement stat;
    ResultSet rs;
    String sql;

    String dataPopup[];
    String data[] = new String[7];
    String[] data_detil = new String[8];
    Menu menu = null ;
    Date Now = new Date();//STATEMENT UNTUK MENIGISI TANGGAL SEKARANG DIJCALENDER
    
    private JasperDesign jasperDesign;
    private JasperReport jasperReport;
    private JasperPrint jasperPrint;
    Map<String, Object> param = new HashMap<String, Object>();

    public Trx_Penjualan(Menu menu) {
        this.menu = menu ;
        initComponents();
        // buat kolom di tabel
        tabel_penjualan.setModel(tblModel);
        Tabel(tabel_penjualan, new int[]{120, 150, 100, 100, 120, 250, 200});
        
        tabel_detil_penjualan.setModel(tblModel2);
        Tabel(tabel_detil_penjualan, new int[]{100, 100, 70, 70, 70, 70, 100, 100});
        dc_tgl_penjualan.setDate(Now);
        // set location
        Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
        setLocation(d.width / 2 - getWidth() / 2, d.height / 6 - getHeight() / 6);
        Validasi();
        
    }

    public void set_autonumber() {
        try {
            con = database.open_a_Connection();
            stat = con.createStatement();
            sql = "SELECT RIGHT(kd_penjualan,3) AS kd FROM tran_penjualan";
            rs = stat.executeQuery(sql);
            if (rs.first() == false) {
                txt_kd_penjualan.setText("PJN001");
            } else {
                rs.last();
                int no = rs.getInt(1) + 1;
                String cno = String.valueOf(no);
                int pjg_cno = cno.length();
                for (int i = 0; i < 3 - pjg_cno; i++) {
                    cno = "0" + cno;
                }
                txt_kd_penjualan.setText("PJN" + cno);
            }
            rs.close();
            stat.close();
            con.close();
        } catch (SQLException error) {
            System.err.println("set_autonumber::"+error);
        }
    }

    private void Validasi() {
        txt_quantity.setDocument(new validasi(4, "0123456789"));// set validasi input
    }
    
    public void tampilkan_data_penjualan() {
        hapus_tabel_penjualan();
        try {
            con = database.open_a_Connection();
            stat = con.createStatement();
            sql = " SELECT * FROM tran_penjualan p, tm_pelanggan pl, tm_chef c "
                + "WHERE p.id_pelanggan=pl.id_pelanggan "
                + "AND p.id_chef=c.id_chef "
                + "ORDER BY p.kd_penjualan DESC ";
            rs = stat.executeQuery(sql);
            while (rs.next()) {
                data[0] = rs.getString("kd_penjualan");
                data[1] = rs.getString("tgl_penjualan");
                data[2] = rs.getString("nm_pelanggan");
                data[3] = rs.getString("nm_chef");
                data[4] = rs.getString("keterangan_status");
                data[5] = rs.getString("konfirmasi_pembayaran");
                data[6] = rs.getString("jam_expired");
                tblModel.addRow(data);
            }
            stat.close();
            con.close();
        } catch (SQLException error) {
            System.err.print("tampilkan_data_penjualan::"+error);
        }
    }
    
    public void set_awal (){
        
        // textfield
        txt_quantity.setEnabled(false);
        
        lbl_stock.setVisible(false);
        txt_stock.setVisible(false);
        
        lbl_proses.setVisible(false);
        
        // radio button
        // rb status stok
        rb_stok_masak.setEnabled(false);
        rb_stok_ready.setEnabled(false);
        // rb status pemasakan
        rb_proses_proses.setVisible(false);
        rb_proses_selesai.setVisible(false);
        // rb status penjualan
        rb_konfirmasi.setEnabled(false);
        rb_belum_konfirmasi.setEnabled(false);
        rb_pengiriman.setEnabled(false);
        rb_lunas.setEnabled(false);
        rb_lunas.setVisible(false);
        
        // button
        btn_tambah.setEnabled(true);
        btn_cari_pelanggan.setEnabled(false);
        btn_cari_barang.setEnabled(false);
        btn_cari_chef.setEnabled(false);
        btn_hitung.setEnabled(false);
        btn_tambah_detil.setEnabled(false);
        btn_kurang_detil.setEnabled(false);
        btn_simpan.setEnabled(false);
        btn_ubah.setEnabled(false);
        btn_cetak.setEnabled(false);
        
    }
    
    public void set_kosong() {
        txt_id_pelanggan.setText("");
        txt_nm_pelanggan.setText("");
        txt_kd_makanan.setText("");
        txt_nm_makanan.setText("");
        txt_kategori.setText("");
        txt_harga_satuan.setText("");
        txt_id_chef.setText("");
        txt_nm_chef.setText("");
        txt_pesan_konfirm.setText("");
        
        txt_stock.setText("");
        txt_quantity.setText("");
        txt_total.setText("");
        
        bg_status.clearSelection();
        bg_makanan.clearSelection();
        bg_proses.clearSelection();
        
        dc_tgl_penjualan.setDate(new Date());
        
        hapus_tabel_detil_penjualan();
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        bg_status = new javax.swing.ButtonGroup();
        bg_makanan = new javax.swing.ButtonGroup();
        bg_proses = new javax.swing.ButtonGroup();
        desktopBackground1 = new style.DesktopBackground();
        jLabel1 = new javax.swing.JLabel();
        tab_form = new javax.swing.JTabbedPane();
        jPanel3 = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        txt_kd_penjualan = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        btn_tambah = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        txt_id_pelanggan = new javax.swing.JTextField();
        btn_cari_pelanggan = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        txt_nm_pelanggan = new javax.swing.JTextField();
        dc_tgl_penjualan = new com.toedter.calendar.JDateChooser();
        jLabel16 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        txt_id_chef = new javax.swing.JTextField();
        txt_nm_chef = new javax.swing.JTextField();
        btn_cari_chef = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        txt_kd_makanan = new javax.swing.JTextField();
        btn_cari_barang = new javax.swing.JButton();
        jLabel7 = new javax.swing.JLabel();
        txt_nm_makanan = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        txt_harga_satuan = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        txt_quantity = new javax.swing.JTextField();
        txt_total = new javax.swing.JTextField();
        btn_hitung = new javax.swing.JButton();
        btn_tambah_detil = new javax.swing.JButton();
        btn_kurang_detil = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tabel_detil_penjualan = new javax.swing.JTable();
        jLabel15 = new javax.swing.JLabel();
        rb_stok_masak = new javax.swing.JRadioButton();
        rb_stok_ready = new javax.swing.JRadioButton();
        jLabel18 = new javax.swing.JLabel();
        txt_kategori = new javax.swing.JTextField();
        lbl_stock = new javax.swing.JLabel();
        txt_stock = new javax.swing.JTextField();
        lbl_proses = new javax.swing.JLabel();
        rb_proses_proses = new javax.swing.JRadioButton();
        rb_proses_selesai = new javax.swing.JRadioButton();
        jPanel5 = new javax.swing.JPanel();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        rb_belum_konfirmasi = new javax.swing.JRadioButton();
        rb_konfirmasi = new javax.swing.JRadioButton();
        rb_lunas = new javax.swing.JRadioButton();
        rb_pengiriman = new javax.swing.JRadioButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        txt_pesan_konfirm = new javax.swing.JTextArea();
        jPanel7 = new javax.swing.JPanel();
        btn_simpan = new javax.swing.JButton();
        btn_ubah = new javax.swing.JButton();
        btn_cetak = new javax.swing.JButton();
        btn_batal = new javax.swing.JButton();
        btn_keluar = new javax.swing.JButton();
        jPanel4 = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        tabel_penjualan = new javax.swing.JTable();
        cmb_cari_penjualan = new javax.swing.JComboBox();
        txt_cari_penjualan = new javax.swing.JTextField();
        btn_cari_penjualan = new javax.swing.JButton();

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("PENJUALAN");
        desktopBackground1.add(jLabel1);
        jLabel1.setBounds(10, 10, 1090, 50);

        tab_form.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N

        jPanel3.setBackground(new java.awt.Color(204, 204, 204));

        jPanel1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel1.setOpaque(false);

        jLabel2.setText("Kode Penjualan");

        txt_kd_penjualan.setEditable(false);

        jLabel3.setText("Tanggal Pesanan");

        btn_tambah.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btn_tambah.setText("Tambah");
        btn_tambah.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_tambahActionPerformed(evt);
            }
        });

        jLabel4.setText("Id Pelanggan");

        txt_id_pelanggan.setEditable(false);

        btn_cari_pelanggan.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btn_cari_pelanggan.setText("Cari");
        btn_cari_pelanggan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_cari_pelangganActionPerformed(evt);
            }
        });

        jLabel5.setText("Nama Pelanggan");

        txt_nm_pelanggan.setEditable(false);

        dc_tgl_penjualan.setEnabled(false);

        jLabel16.setText("Id Chef");

        jLabel17.setText("Nama Chef");

        txt_id_chef.setEditable(false);

        txt_nm_chef.setEditable(false);

        btn_cari_chef.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btn_cari_chef.setText("Cari");
        btn_cari_chef.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_cari_chefActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, 89, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(txt_kd_penjualan, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_tambah))
                    .addComponent(dc_tgl_penjualan, javax.swing.GroupLayout.PREFERRED_SIZE, 205, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(11, 11, 11)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(txt_id_pelanggan, javax.swing.GroupLayout.PREFERRED_SIZE, 164, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btn_cari_pelanggan, javax.swing.GroupLayout.PREFERRED_SIZE, 68, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(txt_nm_pelanggan))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel16, javax.swing.GroupLayout.PREFERRED_SIZE, 72, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(txt_id_chef, javax.swing.GroupLayout.PREFERRED_SIZE, 158, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_cari_chef))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel17, javax.swing.GroupLayout.PREFERRED_SIZE, 62, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(37, 37, 37)
                        .addComponent(txt_nm_chef, javax.swing.GroupLayout.PREFERRED_SIZE, 219, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(26, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel16, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(txt_id_chef, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btn_cari_chef))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txt_id_pelanggan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btn_cari_pelanggan))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel17, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(txt_nm_chef, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(txt_nm_pelanggan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txt_kd_penjualan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btn_tambah))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(dc_tgl_penjualan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel2.setOpaque(false);

        jLabel6.setText("Kode Makanan");

        txt_kd_makanan.setEditable(false);

        btn_cari_barang.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btn_cari_barang.setText("Cari");
        btn_cari_barang.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_cari_barangActionPerformed(evt);
            }
        });

        jLabel7.setText("Nama Makanan");

        txt_nm_makanan.setEditable(false);

        jLabel10.setText("Harga Satuan");

        txt_harga_satuan.setEditable(false);

        jLabel8.setText("Qty");

        jLabel11.setText("Total");

        txt_total.setEditable(false);

        btn_hitung.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btn_hitung.setText("Hitung");
        btn_hitung.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_hitungActionPerformed(evt);
            }
        });

        btn_tambah_detil.setText("+");
        btn_tambah_detil.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_tambah_detilActionPerformed(evt);
            }
        });

        btn_kurang_detil.setText("-");
        btn_kurang_detil.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_kurang_detilActionPerformed(evt);
            }
        });

        tabel_detil_penjualan.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tabel_detil_penjualan.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tabel_detil_penjualanMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tabel_detil_penjualan);

        jLabel15.setText("Status Stok");

        bg_makanan.add(rb_stok_masak);
        rb_stok_masak.setText("Pembuatan");
        rb_stok_masak.setOpaque(false);
        rb_stok_masak.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                rb_stok_masakItemStateChanged(evt);
            }
        });

        bg_makanan.add(rb_stok_ready);
        rb_stok_ready.setText("Stok");
        rb_stok_ready.setOpaque(false);
        rb_stok_ready.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                rb_stok_readyItemStateChanged(evt);
            }
        });

        jLabel18.setText("Kategori");

        txt_kategori.setEditable(false);

        lbl_stock.setText("Sisa Stok");

        txt_stock.setEditable(false);

        lbl_proses.setText("Proses");

        bg_proses.add(rb_proses_proses);
        rb_proses_proses.setText("Proses");
        rb_proses_proses.setOpaque(false);
        rb_proses_proses.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                rb_proses_prosesItemStateChanged(evt);
            }
        });

        bg_proses.add(rb_proses_selesai);
        rb_proses_selesai.setText("Selesai");
        rb_proses_selesai.setOpaque(false);
        rb_proses_selesai.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                rb_proses_selesaiItemStateChanged(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txt_kd_makanan, javax.swing.GroupLayout.PREFERRED_SIZE, 134, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_cari_barang, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel18, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txt_kategori))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txt_harga_satuan))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txt_nm_makanan))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(lbl_stock, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txt_stock, javax.swing.GroupLayout.PREFERRED_SIZE, 53, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLabel8, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(lbl_proses))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(txt_total)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                                .addComponent(txt_quantity, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btn_hitung, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(rb_proses_proses, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(27, 27, 27)
                                .addComponent(rb_proses_selesai, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel15)
                        .addGap(44, 44, 44)
                        .addComponent(rb_stok_masak, javax.swing.GroupLayout.PREFERRED_SIZE, 88, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(rb_stok_ready, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btn_tambah_detil)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_kurang_detil))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 702, Short.MAX_VALUE))
                .addGap(19, 19, 19))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txt_kd_makanan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btn_cari_barang))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txt_nm_makanan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel18, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txt_kategori, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txt_harga_satuan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(10, 10, 10)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lbl_stock, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txt_stock, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel15, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(rb_stok_masak)
                                .addComponent(rb_stok_ready)))
                        .addGap(7, 7, 7)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(txt_quantity, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(btn_hitung))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txt_total, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(37, 37, 37)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lbl_proses, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(rb_proses_proses)
                                .addComponent(rb_proses_selesai))))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btn_tambah_detil)
                            .addComponent(btn_kurang_detil))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)))
                .addContainerGap(23, Short.MAX_VALUE))
        );

        jPanel5.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel5.setOpaque(false);

        jLabel13.setText("Status");

        jLabel14.setText("Keterangan");

        bg_status.add(rb_belum_konfirmasi);
        rb_belum_konfirmasi.setText("Belum Konfirmasi");
        rb_belum_konfirmasi.setOpaque(false);

        bg_status.add(rb_konfirmasi);
        rb_konfirmasi.setText("Konfirmasi");
        rb_konfirmasi.setOpaque(false);

        bg_status.add(rb_lunas);
        rb_lunas.setText("Diterima / Lunas");
        rb_lunas.setOpaque(false);

        bg_status.add(rb_pengiriman);
        rb_pengiriman.setText("Pengiriman / Bayar Ditempat");
        rb_pengiriman.setOpaque(false);

        txt_pesan_konfirm.setColumns(20);
        txt_pesan_konfirm.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N
        txt_pesan_konfirm.setRows(2);
        jScrollPane2.setViewportView(txt_pesan_konfirm);

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, 56, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(41, 41, 41)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(rb_lunas)
                    .addComponent(rb_pengiriman)
                    .addComponent(rb_konfirmasi)
                    .addComponent(rb_belum_konfirmasi))
                .addGap(78, 78, 78)
                .addComponent(jLabel14)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 597, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel5Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(rb_belum_konfirmasi)
                            .addComponent(jLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel14, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(rb_konfirmasi)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(rb_pengiriman)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(rb_lunas)))
                .addGap(15, 15, 15))
        );

        jPanel7.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel7.setOpaque(false);

        btn_simpan.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btn_simpan.setText("Simpan");
        btn_simpan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_simpanActionPerformed(evt);
            }
        });

        btn_ubah.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btn_ubah.setText("Ubah");
        btn_ubah.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_ubahActionPerformed(evt);
            }
        });

        btn_cetak.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btn_cetak.setText("Cetak");
        btn_cetak.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_cetakActionPerformed(evt);
            }
        });

        btn_batal.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btn_batal.setText("Batal");
        btn_batal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_batalActionPerformed(evt);
            }
        });

        btn_keluar.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btn_keluar.setText("Keluar");
        btn_keluar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_keluarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel7Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btn_simpan, javax.swing.GroupLayout.PREFERRED_SIZE, 92, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btn_ubah, javax.swing.GroupLayout.PREFERRED_SIZE, 92, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btn_cetak, javax.swing.GroupLayout.PREFERRED_SIZE, 92, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btn_batal, javax.swing.GroupLayout.PREFERRED_SIZE, 92, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btn_keluar, javax.swing.GroupLayout.PREFERRED_SIZE, 92, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(272, 272, 272))
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(btn_simpan, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btn_ubah)
                            .addComponent(btn_cetak)
                            .addComponent(btn_batal)
                            .addComponent(btn_keluar))))
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(18, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        tab_form.addTab("FORM", jPanel3);

        jPanel4.setBackground(new java.awt.Color(153, 255, 204));

        tabel_penjualan.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tabel_penjualan.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tabel_penjualanMouseClicked(evt);
            }
        });
        jScrollPane3.setViewportView(tabel_penjualan);

        cmb_cari_penjualan.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Kode Penjualan", "Tanggal Penjualan", "Nama Pelanggan" }));

        btn_cari_penjualan.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btn_cari_penjualan.setText("Cari");
        btn_cari_penjualan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_cari_penjualanActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 1065, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(cmb_cari_penjualan, javax.swing.GroupLayout.PREFERRED_SIZE, 138, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txt_cari_penjualan, javax.swing.GroupLayout.PREFERRED_SIZE, 148, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_cari_penjualan, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(cmb_cari_penjualan, javax.swing.GroupLayout.DEFAULT_SIZE, 25, Short.MAX_VALUE)
                    .addComponent(btn_cari_penjualan)
                    .addComponent(txt_cari_penjualan))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 547, Short.MAX_VALUE)
                .addContainerGap())
        );

        tab_form.addTab("DATA", jPanel4);

        desktopBackground1.add(tab_form);
        tab_form.setBounds(10, 60, 1090, 640);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(desktopBackground1, javax.swing.GroupLayout.DEFAULT_SIZE, 1120, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(desktopBackground1, javax.swing.GroupLayout.PREFERRED_SIZE, 782, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        setSize(new java.awt.Dimension(1136, 745));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btn_keluarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_keluarActionPerformed
        dispose();
    }//GEN-LAST:event_btn_keluarActionPerformed

    private void btn_batalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_batalActionPerformed
        set_awal();
        set_kosong();
        set_autonumber();
        tampilkan_data_penjualan();
    }//GEN-LAST:event_btn_batalActionPerformed

    private void btn_ubahActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_ubahActionPerformed
        
        String status = null;
        if (rb_belum_konfirmasi.isSelected()) {
            status = "Belum Konfirmasi";
        } else if (rb_konfirmasi.isSelected()) {
            status = "Konfirmasi";
        } else if (rb_pengiriman.isSelected()) {
            status = "Pengiriman";
        } else if (rb_lunas.isSelected()) {
            status = "Lunas";
        }
        
        System.err.println(status);
        if (status == null) {
            JOptionPane.showMessageDialog(null, "Status pesanan belum dipilih", "Peringatan", JOptionPane.WARNING_MESSAGE);
            return;
        }
        
        try {
            con  = database.open_a_Connection();
            stat = con.createStatement();
            sql  = " UPDATE tran_penjualan SET "
                   + " keterangan_status= '" + status+ "'"
                    + ",konfirmasi_pembayaran='" + txt_pesan_konfirm.getText() + "' " 
                   + " where kd_penjualan= '" + txt_kd_penjualan.getText() + "'" ;
            
            int sukses = stat.executeUpdate(sql);
            if (sukses == 1) {
                for (int i = 0; i < tabel_detil_penjualan.getRowCount(); i++) {
                    // Simpan detil_pesanan
                    Statement stat_makanan = con.createStatement();
                    String simpandetil = "UPDATE tran_detil_penjualan SET "
                            + "status_proses='"+tblModel2.getValueAt(i, 7).toString()+"'"
                            + "WHERE kd_penjualan='"+txt_kd_penjualan.getText()+"' "
                            + "AND kd_makanan='"+tblModel2.getValueAt(i, 0).toString()+"'";
                    int suksesdetil = stat_makanan.executeUpdate(simpandetil);
                    if (suksesdetil == 1) {
                        System.out.println("ubah pesanan detail berhasil");
                    }
                    stat_makanan.close();
                }
                
                tab_form.setSelectedIndex(1);
                JOptionPane.showMessageDialog(null, "Berhasil mengubah data");
                set_awal();
                set_kosong();
                set_autonumber();
                tampilkan_data_penjualan();
            }
            stat.close();
            con.close();
        } catch (SQLException error) {
            System.err.println("btn_ubahActionPerformed::"+error);
        }
        
    }//GEN-LAST:event_btn_ubahActionPerformed

    private void btn_simpanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_simpanActionPerformed
        if (tabel_detil_penjualan.getRowCount() == 0) {
            JOptionPane.showMessageDialog(null, "Belum ada Transaksi", "Peringatan", JOptionPane.WARNING_MESSAGE);
            return;
        }
        
        String status = null;
        if (rb_belum_konfirmasi.isSelected()) {
            status = "Belum Konfirmasi";
        } else if (rb_konfirmasi.isSelected()) {
            status = "Konfirmasi";
        } else if (rb_pengiriman.isSelected()) {
            status = "Pengiriman";
        } else if (rb_lunas.isSelected()) {
            status = "Lunas";
        }
        if (status == null) {
            JOptionPane.showMessageDialog(null, "Status transaksi belum dipilih", "Peringatan", JOptionPane.WARNING_MESSAGE);
            return;
        }
        
        // cari tanggal jam expired
        SimpleDateFormat tglBatasKonfirmasi = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        //cal.add(Calendar.HOUR_OF_DAY, 2); // tambah 2 jam
        cal.add(Calendar.MINUTE, 5); // tambah 5 menit
        String datetimeExpired = tglBatasKonfirmasi.format(cal.getTime());
        
        try {
            con  = database.open_a_Connection();
            stat = con.createStatement();
            sql  = " INSERT INTO tran_penjualan VALUES ("
            + "'" + txt_kd_penjualan.getText() + "',"
            + "'" + new SimpleDateFormat("yyyy-MM-dd").format(dc_tgl_penjualan.getDate()) + "',"
            + "'" + txt_id_pelanggan.getText() + "',"
            + "'" + txt_id_chef.getText() + "',"
            + "'" + datetimeExpired+ "',"
            + "'" + status + "',"
            + "'" + txt_pesan_konfirm.getText() + "')" ;
            
            int sukses = stat.executeUpdate(sql);
            if (sukses == 1) {
                for (int i = 0; i < tabel_detil_penjualan.getRowCount(); i++) {
                    // Simpan detil_pesanan
                    Statement stat_makanan = con.createStatement();
                    String simpandetil = "INSERT INTO tran_detil_penjualan VALUES("
                    + "'" + txt_kd_penjualan.getText() + "',"
                    + "'" + tblModel2.getValueAt(i, 0).toString() + "',"
                    + "'" + tblModel2.getValueAt(i, 4).toString() + "',"
                    + "'" + tblModel2.getValueAt(i, 5).toString() + "',"
                    + "'" + tblModel2.getValueAt(i, 6).toString() + "',"
                    + "'" + tblModel2.getValueAt(i, 7).toString() + "')";
                    int suksesdetil = stat_makanan.executeUpdate(simpandetil);
                    if (suksesdetil == 1) {
                        System.out.println("Simpan pesanan detail berhasil");
                    }
                    stat_makanan.close();
                }

                data[0] = txt_kd_penjualan.getText();
                data[1] = new SimpleDateFormat("yyyy-MM-dd").format(dc_tgl_penjualan.getDate());
                data[2] = txt_nm_pelanggan.getText();
                data[3] = txt_nm_chef.getText();
                data[4] = status;
                data[5] = txt_pesan_konfirm.getText();
                data[6] = datetimeExpired;
                tblModel.addRow(data);
                tab_form.setSelectedIndex(1);
                JOptionPane.showMessageDialog(null, "Data berhasil disimpan");
                set_awal();
                set_kosong();
                set_autonumber();
            }
            rs.close();
            stat.close();
            con.close();
        } catch (SQLException error) {
            System.err.println("btn_simpanActionPerformed::"+error);
        }
    }//GEN-LAST:event_btn_simpanActionPerformed

    int row;
    private void tabel_detil_penjualanMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tabel_detil_penjualanMouseClicked
        if (evt.getClickCount() == 2) {
            row = tabel_detil_penjualan.getSelectedRow();
            // set value
            txt_kd_makanan.setText(tblModel2.getValueAt(row, 0).toString());
            txt_nm_makanan.setText(tblModel2.getValueAt(row, 1).toString());
            txt_kategori.setText(tblModel2.getValueAt(row, 2).toString());
            txt_harga_satuan.setText(tblModel2.getValueAt(row, 3).toString());
            txt_quantity.setText(tblModel2.getValueAt(row, 4).toString());
            txt_total.setText(tblModel2.getValueAt(row, 5).toString());
            txt_stock.setText(menu.cari_data_tabel("tm_makanan", "stock", "kd_makanan", txt_kd_makanan.getText()));
            
            // cek status stok makanan
            if (tblModel2.getValueAt(row, 6).toString().equalsIgnoreCase("Pembuatan")) {
                rb_stok_masak.setSelected(true);
            } else {
                rb_stok_ready.setSelected(true);
            }
            
            // cek status proses pemasakan
            if (tblModel2.getValueAt(row, 7).toString().equalsIgnoreCase("Proses")) {
                rb_proses_proses.setSelected(true);
            } else {
                rb_proses_selesai.setSelected(true);
            }
            
            // status proses pemasakan
            if(btn_simpan.isEnabled()){
                
            }else{
                lbl_proses.setVisible(true);
                rb_proses_proses.setVisible(true);
                rb_proses_selesai.setVisible(true);
            }
            
        }
    }//GEN-LAST:event_tabel_detil_penjualanMouseClicked

    private void btn_tambah_detilActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_tambah_detilActionPerformed
        if(txt_kd_makanan.getText().isEmpty()==false && 
           txt_quantity.getText().isEmpty()==false && 
           txt_total.getText().isEmpty()==false){
            // hapus data yang sama ditabel detil
            for (int i = 0; i < tblModel2.getRowCount(); i++) {
                if(txt_kd_makanan.getText().equalsIgnoreCase(tblModel2.getValueAt(i, 0).toString())){
                    tblModel2.removeRow(i);
                }
            }
            
            String status_makanan = null, status_proses = null;
            if (rb_stok_masak.isSelected()) {
                status_makanan = "Pembuatan";
                status_proses  = "Proses";
            } else if (rb_stok_ready.isSelected()) {
                status_makanan = "Stok";
                status_proses  = "Selesai";
            }

            data_detil[0] = txt_kd_makanan.getText();
            data_detil[1] = txt_nm_makanan.getText();
            data_detil[2] = txt_kategori.getText();
            data_detil[3] = txt_harga_satuan.getText();
            data_detil[4] = txt_quantity.getText();
            data_detil[5] = txt_total.getText();
            data_detil[6] = status_makanan;
            data_detil[7] = status_proses;
            tblModel2.addRow(data_detil);

            txt_kd_makanan.setText("");
            txt_nm_makanan.setText("");
            txt_kategori.setText("");
            txt_harga_satuan.setText("");
            txt_stock.setText("");
            txt_quantity.setText("");
            txt_total.setText("");
        }
    }//GEN-LAST:event_btn_tambah_detilActionPerformed

    private void btn_hitungActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_hitungActionPerformed
        if (txt_quantity.getText().isEmpty()) {
            JOptionPane.showMessageDialog(null, "qty input masih kosong", "peringatan", JOptionPane.WARNING_MESSAGE);
            return;
        }
        
        if (rb_stok_ready.isSelected()) {
            if (Integer.parseInt(txt_quantity.getText()) > Integer.parseInt(txt_stock.getText())) {
                JOptionPane.showMessageDialog(null, "qty input lebih dari sisa stok", "peringatan", JOptionPane.WARNING_MESSAGE);
                txt_quantity.setText("");
                txt_total.setText("");
                return;
            }
        }

        int qty = Integer.valueOf(txt_quantity.getText());
        int harga = Integer.valueOf(txt_harga_satuan.getText());
        int total = qty * harga;
        txt_total.setText(String.valueOf(total));

    }//GEN-LAST:event_btn_hitungActionPerformed

    private void btn_cari_barangActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_cari_barangActionPerformed
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                Popup_Makanan dialog = new Popup_Makanan(new javax.swing.JFrame(), true);
                dialog.setVisible(true);
                if (dialog.getData()[1].equals("")) {
                    return;
                }
                txt_kd_makanan.setText(dialog.getData()[0]);
                txt_nm_makanan.setText(dialog.getData()[1]);
                txt_kategori.setText(dialog.getData()[2]);
                txt_harga_satuan.setText(dialog.getData()[3]);
                txt_stock.setText(dialog.getData()[4]);

                txt_quantity.setEnabled(true);
                
                rb_stok_masak.setEnabled(true);
                rb_stok_ready.setEnabled(true);
                
                rb_konfirmasi.setEnabled(true);
                rb_belum_konfirmasi.setEnabled(true);
                rb_pengiriman.setEnabled(true);
                
                btn_hitung.setEnabled(true);
                btn_simpan.setEnabled(true);
                
                btn_tambah_detil.setEnabled(true);
                btn_kurang_detil.setEnabled(true);
            }
        });
    }//GEN-LAST:event_btn_cari_barangActionPerformed

    private void btn_cari_pelangganActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_cari_pelangganActionPerformed
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                Popup_Pelanggan dialog = new Popup_Pelanggan(new javax.swing.JFrame(), true);
                dialog.setVisible(true);
                if (dialog.getData()[1].equals("")) {
                    return;
                }
                txt_id_pelanggan.setText(dialog.getData()[0]);
                txt_nm_pelanggan.setText(dialog.getData()[1]);

                btn_cari_chef.setEnabled(true);
                btn_cari_pelanggan.setEnabled(false);
            }
        });
    }//GEN-LAST:event_btn_cari_pelangganActionPerformed

    private void btn_tambahActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_tambahActionPerformed
        btn_cari_pelanggan.setEnabled(true);
        btn_tambah.setEnabled(false);
    }//GEN-LAST:event_btn_tambahActionPerformed

    private void btn_cari_penjualanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_cari_penjualanActionPerformed
        hapus_tabel_penjualan();
        String kata_kunci = "";
        if (cmb_cari_penjualan.getSelectedItem().toString().equals("Kode Penjualan")) {
            kata_kunci = "kd_penjualan";
        } else if (cmb_cari_penjualan.getSelectedItem().toString().equals("Tanggal Penjualan")) {
            kata_kunci = "tgl_penjualan";
        } else if (cmb_cari_penjualan.getSelectedItem().toString().equals("Nama Pelanggan")) {
            kata_kunci = "pl.nm_pelanggan";
        }
        try {
            con = database.open_a_Connection();
            stat = con.createStatement();
            sql = "SELECT * FROM tran_penjualan p, tm_chef c, tm_pelanggan pl "
                    + "WHERE p.id_pelanggan=pl.id_pelanggan "
                    + "AND p.id_chef=c.id_chef "
                    + "AND " + kata_kunci + " LIKE '%" + txt_cari_penjualan.getText() + "%' ORDER BY kd_penjualan DESC";
            rs = stat.executeQuery(sql);
            while (rs.next()) {
                data[0] = rs.getString("kd_penjualan");
                data[1] = rs.getString("tgl_penjualan");
                data[2] = rs.getString("nm_pelanggan");
                data[3] = rs.getString("nm_chef");
                data[4] = rs.getString("keterangan_status");
                data[5] = rs.getString("konfirmasi_pembayaran");
                data[6] = rs.getString("jam_expired");
                tblModel.addRow(data);
            }
            stat.close();
            con.close();
        } catch (SQLException error) {
            System.err.print("btn_cari_penjualanActionPerformed::"+error);
        }
    }//GEN-LAST:event_btn_cari_penjualanActionPerformed

    private void tabel_penjualanMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tabel_penjualanMouseClicked
        if (evt.getClickCount() == 2) {
            set_awal();
            set_kosong();
            row = tabel_penjualan.getSelectedRow();
            try {
                con = database.open_a_Connection();
                stat = con.createStatement();
                sql = "SELECT * FROM tran_penjualan p, tm_pelanggan pl, tm_chef c "
                + "WHERE p.id_pelanggan=pl.id_pelanggan "
                + "AND p.id_chef=c.id_chef "
                + "AND p.kd_penjualan='"+tblModel.getValueAt(row, 0).toString()+"' "
                + "ORDER BY p.kd_penjualan DESC";
                rs = stat.executeQuery(sql);
                if (rs.next()) {
                    txt_kd_penjualan.setText(rs.getString("kd_penjualan"));
                    dc_tgl_penjualan.setDate(java.sql.Date.valueOf(rs.getString("tgl_penjualan").split(" ")[0]));
                    txt_id_pelanggan.setText(rs.getString("id_pelanggan"));
                    txt_nm_pelanggan.setText(rs.getString("nm_pelanggan"));
                    txt_id_chef.setText(rs.getString("id_chef"));
                    txt_nm_chef.setText(rs.getString("nm_chef"));
                    if(rs.getString("keterangan_status").equals("Belum Konfirmasi")){
                        rb_belum_konfirmasi.setSelected(true);
                    }else if(rs.getString("keterangan_status").equals("Konfirmasi")){
                        rb_konfirmasi.setSelected(true);
                    }else if(rs.getString("keterangan_status").equals("Pengiriman")){
                        rb_pengiriman.setSelected(true);
                    }else if(rs.getString("keterangan_status").equals("Lunas")){
                        rb_lunas.setSelected(true);
                    }
                    txt_pesan_konfirm.setText(rs.getString("konfirmasi_pembayaran"));
                    // cari detil
                    Statement statDetil = con.createStatement();
                    ResultSet resDetil = statDetil.executeQuery(" SELECT * FROM tran_detil_penjualan dp, tm_makanan b "
                        + " WHERE dp.kd_makanan=b.kd_makanan "
                        + " AND kd_penjualan='"+txt_kd_penjualan.getText()+"' ");
                    while (resDetil.next()) {
                        data_detil[0] = resDetil.getString("kd_makanan");
                        data_detil[1] = resDetil.getString("nm_makanan");
                        data_detil[2] = resDetil.getString("kategori");
                        data_detil[3] = resDetil.getString("harga_satuan");
                        data_detil[4] = resDetil.getString("quantity");
                        data_detil[5] = resDetil.getString("total");
                        data_detil[6] = resDetil.getString("status_stok");
                        data_detil[7] = resDetil.getString("status_proses");
                        tblModel2.insertRow(0, data_detil);
                        }
                    
                    resDetil.close();
                    statDetil.close();

                    // cek status lunas
                    if(rs.getString("keterangan_status").equals("Lunas")){
                        rb_proses_proses.setEnabled(true);
                        rb_proses_selesai.setEnabled(true);
                        rb_pengiriman.setEnabled(false);
                        rb_konfirmasi.setEnabled(false);
                        rb_belum_konfirmasi.setEnabled(false);
                        rb_lunas.setEnabled(false);
                        rb_lunas.setVisible(true);
                        btn_ubah.setEnabled(false);
                    }else{
                        rb_proses_proses.setEnabled(true);
                        rb_proses_selesai.setEnabled(true);
                        rb_pengiriman.setEnabled(true);
                        rb_konfirmasi.setEnabled(true);
                        rb_belum_konfirmasi.setEnabled(true);
                        btn_ubah.setEnabled(true);
                    }

                    // button
                    btn_tambah.setEnabled(false);
                    btn_cetak.setEnabled(true);
                    
                    tab_form.setSelectedIndex(0);
                }
                stat.close();
                con.close();
            } catch (SQLException error) {
                System.err.print("tabel_penjualanMouseClicked::"+error);
            }
        }
    }//GEN-LAST:event_tabel_penjualanMouseClicked

    private void btn_cari_chefActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_cari_chefActionPerformed
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                Popup_Chef dialog = new Popup_Chef(new javax.swing.JFrame(), true);
                dialog.setVisible(true);
                if (dialog.getData()[1].equals("")) {
                    return;
                }
                txt_id_chef.setText(dialog.getData()[0]);
                txt_nm_chef.setText(dialog.getData()[1]);
                btn_cari_barang.setEnabled(true);
                btn_cari_chef.setEnabled(false);
            }
        });
    }//GEN-LAST:event_btn_cari_chefActionPerformed

    private void btn_cetakActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_cetakActionPerformed
        try {
            File file = new File("./src/laporan/cetak_penjualan.jrxml");
            jasperDesign = JRXmlLoader.load(file);
            param.put("kd_penjualan", txt_kd_penjualan.getText());
            jasperReport = JasperCompileManager.compileReport(jasperDesign);
            jasperPrint = JasperFillManager.fillReport(jasperReport, param, database.open_a_Connection());
            JOptionPane.showMessageDialog(null, " Data telah di print '" + txt_kd_penjualan.getText() + "'");
            JasperViewer.viewReport(jasperPrint, false);
        } catch (JRException | SQLException error) {
            JOptionPane.showMessageDialog(null, "Gagal mencetak", "Peringatan", JOptionPane.WARNING_MESSAGE);
            System.err.println("btn_cetakActionPerformed::"+error);
        }
    }//GEN-LAST:event_btn_cetakActionPerformed

    private void btn_kurang_detilActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_kurang_detilActionPerformed
         if(tabel_detil_penjualan.getSelectedRow() > -1){
            tblModel2.removeRow(tabel_detil_penjualan.getSelectedRow());
            
            txt_kd_makanan.setText("");
            txt_nm_makanan.setText("");
            txt_harga_satuan.setText("");
            txt_kategori.setText("");
            txt_stock.setText("");
            txt_quantity.setText("");
            txt_total.setText("");

            btn_cari_barang.requestFocus();
            
        }
    }//GEN-LAST:event_btn_kurang_detilActionPerformed

    private void rb_stok_readyItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_rb_stok_readyItemStateChanged
        if(rb_stok_ready.isSelected()){
            if(btn_ubah.isEnabled() == false){
                lbl_stock.setVisible(true);
                txt_stock.setVisible(true);
                txt_quantity.setText("");
                txt_total.setText("");
                txt_quantity.requestFocus();
            }
        } 
    }//GEN-LAST:event_rb_stok_readyItemStateChanged

    private void rb_stok_masakItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_rb_stok_masakItemStateChanged
        if(rb_stok_masak.isSelected()){
            if(btn_ubah.isEnabled() == false){
                lbl_stock.setVisible(false);
                txt_stock.setVisible(false);
                txt_quantity.setText("");
                txt_total.setText("");
                txt_quantity.requestFocus();
            }
        }
    }//GEN-LAST:event_rb_stok_masakItemStateChanged

    private void rb_proses_prosesItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_rb_proses_prosesItemStateChanged
        if(rb_proses_proses.isSelected()){
            if(btn_ubah.isEnabled()){
                tblModel2.setValueAt("Proses", row, 7);
            }
        }
    }//GEN-LAST:event_rb_proses_prosesItemStateChanged

    private void rb_proses_selesaiItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_rb_proses_selesaiItemStateChanged
        if(rb_proses_selesai.isSelected()){
            if(btn_ubah.isEnabled()){
                tblModel2.setValueAt("Selesai", row, 7);
            }
        }
    }//GEN-LAST:event_rb_proses_selesaiItemStateChanged
    
    public void setDataPopup(String[] dataPopup) {
        this.dataPopup = dataPopup;
    }

    public String[] getDataPopup() {
        return dataPopup;
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup bg_makanan;
    private javax.swing.ButtonGroup bg_proses;
    private javax.swing.ButtonGroup bg_status;
    private javax.swing.JButton btn_batal;
    private javax.swing.JButton btn_cari_barang;
    private javax.swing.JButton btn_cari_chef;
    private javax.swing.JButton btn_cari_pelanggan;
    private javax.swing.JButton btn_cari_penjualan;
    private javax.swing.JButton btn_cetak;
    private javax.swing.JButton btn_hitung;
    private javax.swing.JButton btn_keluar;
    private javax.swing.JButton btn_kurang_detil;
    private javax.swing.JButton btn_simpan;
    private javax.swing.JButton btn_tambah;
    private javax.swing.JButton btn_tambah_detil;
    private javax.swing.JButton btn_ubah;
    private javax.swing.JComboBox cmb_cari_penjualan;
    private com.toedter.calendar.JDateChooser dc_tgl_penjualan;
    private style.DesktopBackground desktopBackground1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JLabel lbl_proses;
    private javax.swing.JLabel lbl_stock;
    private javax.swing.JRadioButton rb_belum_konfirmasi;
    private javax.swing.JRadioButton rb_konfirmasi;
    private javax.swing.JRadioButton rb_lunas;
    private javax.swing.JRadioButton rb_pengiriman;
    private javax.swing.JRadioButton rb_proses_proses;
    private javax.swing.JRadioButton rb_proses_selesai;
    private javax.swing.JRadioButton rb_stok_masak;
    private javax.swing.JRadioButton rb_stok_ready;
    private javax.swing.JTabbedPane tab_form;
    private javax.swing.JTable tabel_detil_penjualan;
    private javax.swing.JTable tabel_penjualan;
    private javax.swing.JTextField txt_cari_penjualan;
    private javax.swing.JTextField txt_harga_satuan;
    private javax.swing.JTextField txt_id_chef;
    private javax.swing.JTextField txt_id_pelanggan;
    private javax.swing.JTextField txt_kategori;
    private javax.swing.JTextField txt_kd_makanan;
    private javax.swing.JTextField txt_kd_penjualan;
    private javax.swing.JTextField txt_nm_chef;
    private javax.swing.JTextField txt_nm_makanan;
    private javax.swing.JTextField txt_nm_pelanggan;
    private javax.swing.JTextArea txt_pesan_konfirm;
    private javax.swing.JTextField txt_quantity;
    private javax.swing.JTextField txt_stock;
    private javax.swing.JTextField txt_total;
    // End of variables declaration//GEN-END:variables
    
    // FUNGSI TABEL
    private void Tabel(javax.swing.JTable tb, int lebar[]) {
        tb.setAutoResizeMode(tb.AUTO_RESIZE_OFF);
        int kolom = tb.getColumnCount();
        for (int i = 0; i < kolom; i++) {
            javax.swing.table.TableColumn tbc = tb.getColumnModel().getColumn(i);
            tbc.setPreferredWidth(lebar[i]);
            tb.setRowHeight(17);
        }
    }

    private void hapus_tabel_penjualan() {
        int rowCount = tabel_penjualan.getRowCount();
        for (int i = 0; i < rowCount; i++) {
            tblModel.removeRow(0);
        }
    }
    private final javax.swing.table.DefaultTableModel tblModel = getDefaultTableModel();

    private javax.swing.table.DefaultTableModel getDefaultTableModel() {
        return new javax.swing.table.DefaultTableModel(
                new Object[][]{},
                new String[]{"Kode Penjualan", "Tanggal Penjualan", "Pelanggan", "Chef", "Status Konfirmasi", "Pesan Konfirmasi", "Expired"}
        ) {
            boolean[] canEdit = new boolean[]{
                false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit[columnIndex];
            }
        };
    }

    private final javax.swing.table.DefaultTableModel tblModel2 = getDefaultTableModel2();

    private javax.swing.table.DefaultTableModel getDefaultTableModel2() {
        return new javax.swing.table.DefaultTableModel(
                new Object[][]{},
                new String[]{"Kode Makanan", "Nama Makanan", "Kategori" , "Harga", "Quantity", "Total", "Status Stok", "Status Proses"}
        ) {
            boolean[] canEdit = new boolean[]{
                false, false, false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit[columnIndex];
            }
        };
    }

    private void hapus_tabel_detil_penjualan() {
        int rowCount = tabel_detil_penjualan.getRowCount();
        for (int i = 0; i < rowCount; i++) {
            tblModel2.removeRow(0);
        }
    }
}
