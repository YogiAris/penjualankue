package penjualan;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;
import koneksi.koneksi;


public class Popup_Penjualan extends javax.swing.JDialog {
    koneksi database = new koneksi();
    Connection con;
    Statement stat;
    ResultSet rs;
    String sql;
    String data[] = new String[5];
    
    Trx_Penjualan kwitansi = new Trx_Penjualan(null);
   
    public Popup_Penjualan(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        tabel_penjualan.setModel(tblModel);
        Tabel(tabel_penjualan, new int[]{150, 150, 150, 150, 150});
        tampilkan_data_penjualan();
    }
    
      private void tampilkan_data_penjualan() {
        try {
            con  = database.open_a_Connection();
            stat = con.createStatement();
            sql  = "SELECT * FROM tran_penjualan p, tm_pelanggan pl WHERE p.id_pelanggan=pl.id_pelanggan AND p.keterangan_status='Pengiriman' ORDER BY p.kd_penjualan DESC";
            rs  = stat.executeQuery(sql);
            while (rs.next()) {
                data[0] = rs.getString("kd_penjualan");
                data[1] = rs.getString("tgl_penjualan");
                data[2] = rs.getString("nm_pelanggan");
                data[3] = rs.getString("keterangan_status");
                data[4] = rs.getString("konfirmasi_pembayaran");
                tblModel.addRow(data);
            }
            stat.close();
            con.close();
        } catch (SQLException error) {
            System.err.print(error);
        }
    }

    public String[] getData() {
        return kwitansi.getDataPopup();
    }
      
      

   
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        cmb_cari_penjualan = new javax.swing.JComboBox();
        txt_cari_penjualan = new javax.swing.JTextField();
        btn_cari_penjualan = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tabel_penjualan = new javax.swing.JTable();
        btn_tambah = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(204, 204, 204));

        cmb_cari_penjualan.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Kode Penjualan", "Tanggal Penjualan", "Pelanggan", " ", " " }));

        btn_cari_penjualan.setText("Cari");
        btn_cari_penjualan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_cari_penjualanActionPerformed(evt);
            }
        });

        tabel_penjualan.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tabel_penjualan);

        btn_tambah.setText("Pilih");
        btn_tambah.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_tambahActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(cmb_cari_penjualan, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txt_cari_penjualan, javax.swing.GroupLayout.PREFERRED_SIZE, 181, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btn_cari_penjualan, javax.swing.GroupLayout.DEFAULT_SIZE, 97, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btn_tambah, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cmb_cari_penjualan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btn_cari_penjualan)
                    .addComponent(txt_cari_penjualan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btn_tambah))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 212, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents
    private int row ;
    private void btn_tambahActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_tambahActionPerformed
        row = tabel_penjualan.getSelectedRow();
        if (row >= 0) {
            kwitansi.setDataPopup(new String[]{
                tblModel.getValueAt(row, 0).toString(),
                tblModel.getValueAt(row, 1).toString()});
            dispose();
        } else {
            JOptionPane.showMessageDialog(null, "Pilih salah satu dalam tabel", "Peringatan", JOptionPane.WARNING_MESSAGE);
        }
    }//GEN-LAST:event_btn_tambahActionPerformed

    private void btn_cari_penjualanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_cari_penjualanActionPerformed
         set_kosong_tabel();
        String kata_kunci = "";
        if (cmb_cari_penjualan.getSelectedItem().toString().equals("KODE")) {
            kata_kunci = "kd_pesanan";
        } else if (cmb_cari_penjualan.getSelectedItem().toString().equals("TANGGAL")) {
            kata_kunci = "tgl_pesanan";
        } else if (cmb_cari_penjualan.getSelectedItem().toString().equals("PELANGGAN")) {
            kata_kunci = "pl.nm_pelanggan";
        }
        try {
            con = database.open_a_Connection();
            stat = con.createStatement();
            sql = "SELECT * FROM tran_penjualan p, tm_pelanggan pl WHERE p.id_pelanggan=pl.id_pelanggan AND p.status='Pengiriman' AND " + kata_kunci + " LIKE '%" + txt_cari_penjualan.getText() + "%' ORDER BY kd_penjualan DESC";
            rs = stat.executeQuery(sql);
            while (rs.next()) {
                data[0] = rs.getString("kd_penjualan");
                data[1] = rs.getString("tgl_penjualan");
                data[2] = rs.getString("nm_pelanggan");
                data[3] = rs.getString("keterangan_status");
                data[4] = rs.getString("konfirmasi_pembayaran");
                tblModel.addRow(data);
            }
            stat.close();
            con.close();
        } catch (SQLException error) {
            System.err.print(error);
        }
    }//GEN-LAST:event_btn_cari_penjualanActionPerformed

    
   
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_cari_penjualan;
    private javax.swing.JButton btn_tambah;
    private javax.swing.JComboBox cmb_cari_penjualan;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tabel_penjualan;
    private javax.swing.JTextField txt_cari_penjualan;
    // End of variables declaration//GEN-END:variables
    // FUNGSI TABEL
    private void Tabel(javax.swing.JTable tb, int lebar[]) {
        tb.setAutoResizeMode(tb.AUTO_RESIZE_OFF);
        int kolom = tb.getColumnCount();
        for (int i = 0; i < kolom; i++) {
            javax.swing.table.TableColumn tbc = tb.getColumnModel().getColumn(i);
            tbc.setPreferredWidth(lebar[i]);
            tb.setRowHeight(17);
        }
    }
    private void set_kosong_tabel() {
        int rowCount = tabel_penjualan.getRowCount();
        for (int i = 0; i < rowCount; i++) {
            tblModel.removeRow(0);
        }
    }
    private final javax.swing.table.DefaultTableModel tblModel = getDefaultTableModel();
    private javax.swing.table.DefaultTableModel getDefaultTableModel() {
        return new javax.swing.table.DefaultTableModel(
                new Object[][]{},
                new String[]{"Kode Penjualan", "Tanggal Penjualan", "Pelanggan", "Status", "Konfirmasi"}
        ) {
            boolean[] canEdit = new boolean[]{
                false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit[columnIndex];
            }
        };
    }
}
