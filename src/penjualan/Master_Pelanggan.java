package penjualan;

import fungsi.validasi;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import koneksi.koneksi;

public class Master_Pelanggan extends javax.swing.JDialog {

    koneksi database = new koneksi();
    Connection con;
    Statement stat;
    ResultSet rs;
    String sql;
    String data[] = new String[4];
    Menu menu = null ;

    public Master_Pelanggan(Menu menu) {
        this.menu = menu ;
        initComponents();
        table_pelanggan.setModel(tblModel);//membuat tabel
        Tabel(table_pelanggan, new int[]{85, 110, 110, 90});
        // set location
        Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
        setLocation(d.width / 2 - getWidth() / 2, d.height / 6 - getHeight() / 6);
        tampilkan_data_pelanggan();
        set_autonumber();
        Validasi();
        set_awal();
    }

    private void tampilkan_data_pelanggan() {
        try {
            con = database.open_a_Connection();
            stat = con.createStatement();
            sql = "SELECT * FROM tm_pelanggan ORDER BY id_pelanggan DESC";
            rs = stat.executeQuery(sql);
            while (rs.next()) {
                data[0] = rs.getString("id_pelanggan");
                data[1] = rs.getString("nm_pelanggan");
                data[2] = rs.getString("no_handphone");
                data[3] = rs.getString("alamat");
                tblModel.addRow(data);
            }
            stat.close();
            con.close();
        } catch (SQLException error) {
            System.err.print(error);
        }
    }
    
    private void set_autonumber() {
        try {
            con  = database.open_a_Connection();
            stat = con.createStatement();
            sql  = "SELECT RIGHT(id_pelanggan,3) AS kd FROM tm_pelanggan";
            rs  = stat.executeQuery(sql);
            if (rs.first() == false) {
                txt_id_pelanggan.setText("P001");
            } else {
                rs.last();
                int no = rs.getInt(1) + 1;
                String cno = String.valueOf(no);
                int pjg_cno = cno.length();
                for (int i = 0; i < 3 - pjg_cno; i++) {
                    cno = "0" + cno;
                }
                txt_id_pelanggan.setText("P" + cno);
            }
            rs.close();
            stat.close();
            con.close();
        } catch (SQLException error) {
            System.out.println(error);
        }
    }

    private void set_awal() {
        btn_simpan.setEnabled(true);
        btn_ubah.setEnabled(false);
        btn_batal.setEnabled(false);
        btn_hapus.setEnabled(false);
        btn_keluar.setEnabled(true);

        txt_nama_pelanggan.requestFocus();
        txt_id_pelanggan.setEnabled(false);
        txt_no_hp.setText("62");
    }

    private void set_kosong() {
        txt_nama_pelanggan.setText("");
        txt_no_hp.setText("62");
        txt_alamat.setText("");
    }
    
    private void Validasi(){
        txt_no_hp.setDocument(new validasi(14, "0123456789"));// set validasi input
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        desktopBackground1 = new style.DesktopBackground();
        jLabel1 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        txt_nama_pelanggan = new javax.swing.JTextField();
        txt_no_hp = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        btn_simpan = new javax.swing.JButton();
        btn_ubah = new javax.swing.JButton();
        btn_hapus = new javax.swing.JButton();
        btn_batal = new javax.swing.JButton();
        btn_keluar = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        txt_alamat = new javax.swing.JTextArea();
        jLabel4 = new javax.swing.JLabel();
        txt_id_pelanggan = new javax.swing.JTextField();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        table_pelanggan = new javax.swing.JTable();
        cmb_cari_pelanggan = new javax.swing.JComboBox();
        txt_cari_pelanggan = new javax.swing.JTextField();
        btn_cari = new javax.swing.JButton();

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("PELANGGAN");
        desktopBackground1.add(jLabel1);
        jLabel1.setBounds(4, 4, 460, 50);

        jPanel1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel1.setOpaque(false);

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel2.setText("Nama Pelanggan");

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel3.setText("No HandPhone");

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel5.setText("Alamat");

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Button", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 11))); // NOI18N
        jPanel3.setOpaque(false);

        btn_simpan.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btn_simpan.setText("Simpan");
        btn_simpan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_simpanActionPerformed(evt);
            }
        });

        btn_ubah.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btn_ubah.setText("Ubah");
        btn_ubah.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_ubahActionPerformed(evt);
            }
        });

        btn_hapus.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btn_hapus.setText("Hapus");
        btn_hapus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_hapusActionPerformed(evt);
            }
        });

        btn_batal.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btn_batal.setText("Batal");
        btn_batal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_batalActionPerformed(evt);
            }
        });

        btn_keluar.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btn_keluar.setText("Keluar");
        btn_keluar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_keluarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btn_simpan)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btn_ubah)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btn_hapus)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btn_batal)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btn_keluar)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btn_simpan)
                    .addComponent(btn_ubah)
                    .addComponent(btn_hapus)
                    .addComponent(btn_batal)
                    .addComponent(btn_keluar))
                .addGap(0, 11, Short.MAX_VALUE))
        );

        txt_alamat.setColumns(20);
        txt_alamat.setLineWrap(true);
        txt_alamat.setRows(5);
        jScrollPane2.setViewportView(txt_alamat);

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel4.setText("Id Pelanggan");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(37, 37, 37)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel5, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel4, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(60, 60, 60)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane2)
                            .addComponent(txt_no_hp)
                            .addComponent(txt_nama_pelanggan)
                            .addComponent(txt_id_pelanggan)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addGap(30, 30, 30))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txt_id_pelanggan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txt_nama_pelanggan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txt_no_hp, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        desktopBackground1.add(jPanel1);
        jPanel1.setBounds(20, 60, 420, 260);

        jPanel2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel2.setOpaque(false);

        table_pelanggan.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        table_pelanggan.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                table_pelangganMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(table_pelanggan);

        cmb_cari_pelanggan.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Id Pelanggan", "Nama Pelanggan", "No Handphone" }));

        btn_cari.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btn_cari.setText("Cari");
        btn_cari.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_cariActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(cmb_cari_pelanggan, javax.swing.GroupLayout.PREFERRED_SIZE, 139, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txt_cari_pelanggan)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_cari))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txt_cari_pelanggan)
                        .addComponent(btn_cari, javax.swing.GroupLayout.DEFAULT_SIZE, 25, Short.MAX_VALUE))
                    .addComponent(cmb_cari_pelanggan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 255, Short.MAX_VALUE)
                .addContainerGap())
        );

        desktopBackground1.add(jPanel2);
        jPanel2.setBounds(20, 330, 420, 310);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(desktopBackground1, javax.swing.GroupLayout.DEFAULT_SIZE, 467, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(desktopBackground1, javax.swing.GroupLayout.DEFAULT_SIZE, 649, Short.MAX_VALUE)
        );

        setSize(new java.awt.Dimension(483, 688));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btn_keluarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_keluarActionPerformed
        dispose();
    }//GEN-LAST:event_btn_keluarActionPerformed

    private void btn_simpanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_simpanActionPerformed
        if (txt_nama_pelanggan.getText().isEmpty()
                | txt_no_hp.getText().isEmpty()
                | txt_alamat.getText().isEmpty()) {
            JOptionPane.showMessageDialog(null, "Form masih ada yang kosong", " Peringatan ", JOptionPane.WARNING_MESSAGE);
            return;
        }
        try {
            con = database.open_a_Connection();
            
            stat = con.createStatement();
            sql = " INSERT INTO tm_pelanggan VALUES ('" + txt_id_pelanggan.getText() + "',"
                    + "'" + txt_nama_pelanggan.getText() + "',"
                    + "'" + txt_no_hp.getText() + "',"
                    + "'" + txt_alamat.getText() + "')";
            int sukses = stat.executeUpdate(sql);
            if (sukses == 1) {
                JOptionPane.showMessageDialog(null, "Berhasil simpan data", " Informasi ", JOptionPane.INFORMATION_MESSAGE);
                String pesan = "Selamat " + txt_nama_pelanggan.getText() + " Anda terdaftar sebagai pelanggan, pada " + new SimpleDateFormat("dd MMMM yyyy HH:mm:ss").format(new Date())+". ";
                set_awal();
                set_kosong();
                hapus_tabel_pelanggan();
                tampilkan_data_pelanggan();
                set_autonumber();
            }
            stat.close();
            con.close();
        } catch (SQLException error) {
            System.err.println(error);
        }
    }//GEN-LAST:event_btn_simpanActionPerformed

    private void btn_ubahActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_ubahActionPerformed
        if (txt_nama_pelanggan.getText().isEmpty()
                | txt_no_hp.getText().isEmpty()
                | txt_alamat.getText().isEmpty()) {
            JOptionPane.showMessageDialog(null, "Form masih ada yang kosong", "Peringatan", JOptionPane.WARNING_MESSAGE);
            return;
        }
        try {
            con = database.open_a_Connection();
            stat = con.createStatement();
            sql = " UPDATE tm_pelanggan SET "
                    + "nm_pelanggan='" + txt_nama_pelanggan.getText() + "',"
                    + "no_handphone='" + txt_no_hp.getText() + "',"
                    + "alamat='" + txt_alamat.getText() + "'"
                    + "WHERE id_pelanggan='" + txt_id_pelanggan.getText() + "'";
            int sukses = stat.executeUpdate(sql);
            if (sukses == 1) {
                JOptionPane.showMessageDialog(null, "Data berhasil diubah", "INFORMASI", JOptionPane.INFORMATION_MESSAGE);
                set_awal();
                set_kosong();
                hapus_tabel_pelanggan();
                tampilkan_data_pelanggan();
            }
            stat.close();
            con.close();
        } catch (SQLException error) {
            System.err.print(error);
        }
    }//GEN-LAST:event_btn_ubahActionPerformed

    private void btn_hapusActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_hapusActionPerformed
        try {
            int jawab = JOptionPane.showConfirmDialog(null, "Anda yakin menghapus Data?", "Konfirmasi", JOptionPane.YES_NO_OPTION);
            if (jawab == 0) {
                con = database.open_a_Connection();
                stat = con.createStatement();
                sql = "DELETE FROM tm_pelanggan WHERE id_pelanggan='" + txt_id_pelanggan.getText() + "'";
                int sukses = stat.executeUpdate(sql);
                if (sukses == 1) {
                    JOptionPane.showMessageDialog(null, "Data berhasil dihapus", "INFORMASI", JOptionPane.INFORMATION_MESSAGE);
                    tblModel.removeRow(row);
                    set_awal();
                    set_kosong();
                }
            }
            con.close();
            stat.close();
        } catch (SQLException error) {
            System.out.println(error);
            JOptionPane.showMessageDialog(null, "Data gagal di hapus" + error, "Peringatan", JOptionPane.WARNING_MESSAGE);
        }
    }//GEN-LAST:event_btn_hapusActionPerformed

    private void btn_batalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_batalActionPerformed
        set_kosong();
        set_awal();
    }//GEN-LAST:event_btn_batalActionPerformed

    private void btn_cariActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_cariActionPerformed
        hapus_tabel_pelanggan();
        String kata_kunci = "";
        switch (cmb_cari_pelanggan.getSelectedItem().toString()) {
            case "Id Pelanggan":
                kata_kunci = "id_pelanggan";
                break;
            case "Nama Pelanggan":
                kata_kunci = "nm_pelanggan";
                break;
            case "No Handphone":
                kata_kunci = "no_handphone";
                break;
        }
        try {
            con = database.open_a_Connection();
            stat = con.createStatement();
            sql = "SELECT * FROM tm_pelanggan WHERE " + kata_kunci + " LIKE '%" + txt_cari_pelanggan.getText() + "%' ORDER BY id_pelanggan DESC";
            rs = stat.executeQuery(sql);
            while (rs.next()) {
                data[0] = rs.getString("id_pelanggan");
                data[1] = rs.getString("nm_pelanggan");
                data[2] = rs.getString("no_handphone");
                tblModel.addRow(data);
            }
            stat.close();
            con.close();
        } catch (SQLException error) {
            System.err.print(error);
        }
    }//GEN-LAST:event_btn_cariActionPerformed
    private int row;
    private void table_pelangganMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_table_pelangganMouseClicked
        if (evt.getClickCount() == 2) {
            row = table_pelanggan.getSelectedRow();
            txt_id_pelanggan.setText(tblModel.getValueAt(row, 0).toString());
            txt_nama_pelanggan.setText(tblModel.getValueAt(row, 1).toString());
            txt_no_hp.setText(tblModel.getValueAt(row, 2).toString());
            txt_alamat.setText(tblModel.getValueAt(row, 3).toString());

            btn_simpan.setEnabled(false);
            btn_hapus.setEnabled(true);
            btn_ubah.setEnabled(true);
            btn_batal.setEnabled(true);
            btn_batal.setEnabled(true);
        }
    }//GEN-LAST:event_table_pelangganMouseClicked


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_batal;
    private javax.swing.JButton btn_cari;
    private javax.swing.JButton btn_hapus;
    private javax.swing.JButton btn_keluar;
    private javax.swing.JButton btn_simpan;
    private javax.swing.JButton btn_ubah;
    private javax.swing.JComboBox cmb_cari_pelanggan;
    private style.DesktopBackground desktopBackground1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable table_pelanggan;
    private javax.swing.JTextArea txt_alamat;
    private javax.swing.JTextField txt_cari_pelanggan;
    private javax.swing.JTextField txt_id_pelanggan;
    private javax.swing.JTextField txt_nama_pelanggan;
    private javax.swing.JTextField txt_no_hp;
    // End of variables declaration//GEN-END:variables
    private void Tabel(javax.swing.JTable tb, int lebar[]) {
        tb.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        int kolom = tb.getColumnCount();
        for (int i = 0; i < kolom; i++) {
            javax.swing.table.TableColumn tbc = tb.getColumnModel().getColumn(i);
            tbc.setPreferredWidth(lebar[i]);
            tb.setRowHeight(17);
        }
    }

    private void hapus_tabel_pelanggan() {
        int rowCount = table_pelanggan.getRowCount();
        for (int i = 0; i < rowCount; i++) {
            tblModel.removeRow(0);
        }
    }

    private final javax.swing.table.DefaultTableModel tblModel = getDefaultTableModel();

    private javax.swing.table.DefaultTableModel getDefaultTableModel() {
        return new javax.swing.table.DefaultTableModel(
                new Object[][]{},
                new String[]{"Id Pelangga", "Nama Pelanggan", "Nomor Handphone", "Alamat"}
        ) {
            boolean[] canEdit = new boolean[]{
                false, false, false, false
            };

            @Override
            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit[columnIndex];
            }
        };
    }
}
