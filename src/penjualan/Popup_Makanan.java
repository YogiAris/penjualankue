package penjualan;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;
import koneksi.koneksi;

public class Popup_Makanan extends javax.swing.JDialog {

    koneksi database = new koneksi();
    Connection con;
    Statement stat;
    ResultSet rs;
    String sql;
    String data[] = new String[7];
    Trx_Penjualan dm = new Trx_Penjualan(null);

    public Popup_Makanan(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        tabel_makanan.setModel(tblModel);//membuat tabel
        Tabel(tabel_makanan, new int[]{80, 120, 100, 100, 80, 80, 80});
        tampilkan_data_makanan();
    }

    private void tampilkan_data_makanan() {
        try {
            con = database.open_a_Connection();
            stat = con.createStatement();
            sql = "SELECT * FROM tm_makanan ORDER BY kd_makanan DESC";
            rs = stat.executeQuery(sql);
            while (rs.next()) {
                data[0] = rs.getString("kd_makanan");
                data[1] = rs.getString("nm_makanan");
                data[2] = rs.getString("kategori");
                data[3] = rs.getString("harga_satuan");
                data[4] = rs.getString("satuan");
                data[5] = rs.getString("ukuran");
                data[6] = rs.getString("stock");
                tblModel.addRow(data);
            }
            stat.close();
            con.close();
        } catch (SQLException error) {
            System.err.print(error);
        }
    }

    public String[] getData() {
        return dm.getDataPopup();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        panel_inbox = new javax.swing.JPanel();
        cmb_cari_makanan = new javax.swing.JComboBox();
        txt_cari_makanan = new javax.swing.JTextField();
        btncari = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        tabel_makanan = new javax.swing.JTable();
        btnpilih = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        panel_inbox.setBackground(new java.awt.Color(204, 204, 204));

        cmb_cari_makanan.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Kode Makanan", "Nama Makanan", "Kategori" }));

        btncari.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btncari.setText("Cari");
        btncari.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btncariActionPerformed(evt);
            }
        });

        tabel_makanan.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        jScrollPane2.setViewportView(tabel_makanan);

        btnpilih.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnpilih.setText("Pilih");
        btnpilih.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnpilihActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panel_inboxLayout = new javax.swing.GroupLayout(panel_inbox);
        panel_inbox.setLayout(panel_inboxLayout);
        panel_inboxLayout.setHorizontalGroup(
            panel_inboxLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panel_inboxLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panel_inboxLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 576, Short.MAX_VALUE)
                    .addGroup(panel_inboxLayout.createSequentialGroup()
                        .addComponent(cmb_cari_makanan, javax.swing.GroupLayout.PREFERRED_SIZE, 143, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txt_cari_makanan)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btncari, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnpilih, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        panel_inboxLayout.setVerticalGroup(
            panel_inboxLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panel_inboxLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panel_inboxLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(cmb_cari_makanan)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, panel_inboxLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btnpilih)
                        .addComponent(btncari, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(txt_cari_makanan, javax.swing.GroupLayout.Alignment.LEADING))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 227, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panel_inbox, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panel_inbox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btncariActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btncariActionPerformed
        hapus_tabel_makanan();
        String kata_kunci = "";
        if (cmb_cari_makanan.getSelectedItem().toString().equals("Kode Makanan")) {
            kata_kunci = "kd_makanan";
        } else if (cmb_cari_makanan.getSelectedItem().toString().equals("Nama Makanan")) {
            kata_kunci = "nm_makanan";
        } else if (cmb_cari_makanan.getSelectedItem().toString().equals("Kategori")) {
            kata_kunci = "kategori";
        }
        try {
            con = database.open_a_Connection();
            stat = con.createStatement();
            sql = "SELECT * FROM tm_makanan WHERE " + kata_kunci + " LIKE '%" + txt_cari_makanan.getText() + "%' ORDER BY kd_makanan DESC";
            rs = stat.executeQuery(sql);
            while (rs.next()) {
                data[0] = rs.getString(1);
                data[1] = rs.getString(2);
                data[2] = rs.getString(3);
                tblModel.addRow(data);
            }
            stat.close();
            con.close();
        } catch (SQLException error) {
            System.err.print(error);
        }
    }//GEN-LAST:event_btncariActionPerformed

    private void btnpilihActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnpilihActionPerformed
        int row = tabel_makanan.getSelectedRow();
        if (row >= 0) {
            dm.setDataPopup(new String[]{
                tblModel.getValueAt(row, 0).toString(),
                tblModel.getValueAt(row, 1).toString(),
                tblModel.getValueAt(row, 2).toString(),
                tblModel.getValueAt(row, 3).toString(),
                tblModel.getValueAt(row, 6).toString()});
            dispose();
        } else {
            JOptionPane.showMessageDialog(null, "Pilih salah satu dalam tabel", "Peringatan", JOptionPane.WARNING_MESSAGE);
        }
    }//GEN-LAST:event_btnpilihActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btncari;
    private javax.swing.JButton btnpilih;
    private javax.swing.JComboBox cmb_cari_makanan;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JPanel panel_inbox;
    private javax.swing.JTable tabel_makanan;
    private javax.swing.JTextField txt_cari_makanan;
    // End of variables declaration//GEN-END:variables
    
    private void hapus_tabel_makanan() {
        int rowCount = tabel_makanan.getRowCount();
        for (int i = 0; i < rowCount; i++) {
            tblModel.removeRow(0);
        }
    }

    private void Tabel(javax.swing.JTable tb, int lebar[]) {
        tb.setAutoResizeMode(tb.AUTO_RESIZE_OFF);
        int kolom = tb.getColumnCount();
        for (int i = 0; i < kolom; i++) {
            javax.swing.table.TableColumn tbc = tb.getColumnModel().getColumn(i);
            tbc.setPreferredWidth(lebar[i]);
            tb.setRowHeight(17);
        }
    }

    private javax.swing.table.DefaultTableModel tblModel = getDefaultTableModel();

    private javax.swing.table.DefaultTableModel getDefaultTableModel() {
        return new javax.swing.table.DefaultTableModel(
                new Object[][]{},
                new String[]{"Kode Makanan", "Nama Makanan", "Kategori", "Harga Satuan", "Satuan"," Ukuran ", "Stock"}
        ) {
            boolean[] canEdit = new boolean[]{
                false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit[columnIndex];
            }
        };
    }
}
